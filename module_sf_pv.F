MODULE  module_sf_pv


  USE module_driver_constants, ONLY : max_domains
  USE module_model_constants, ONLY :  piconst


  USE module_llxy
  USE module_dm, ONLY : wrf_dm_min_real
  USE module_configure, ONLY : grid_config_rec_type

  IMPLICIT NONE

  REAL, DIMENSION(:), ALLOCATABLE :: frcpv_1D, eff_pv_1D, em_module_down_1D
  REAL, DIMENSION(:), ALLOCATABLE :: em_module_up_1D, PV_albedo_min_1D
  REAL, DIMENSION(:), ALLOCATABLE :: ground_alb_1D, ground_lai_1D
  REAL, DIMENSION(:), ALLOCATABLE :: ground_emiss_1D, ground_smois_1D

  INTEGER           :: nt
  INTEGER, DIMENSION(:), ALLOCATABLE :: NKIND
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: ival,jval

 CONTAINS



!! fix these inputs , dimensions

!! add a loop

!! outputs

!! bring the ij dimensions etc

SUBROUTINE PV_UCRC ( SWDOWN ,SWDDIF   ,OMG_URB2D,   &
             COSZ_URB2D,DECLIN_URB    , GLW    ,    &
             T3D         ,TSK,                &
             U_PHY          ,V_PHY    ,             &
             P8w3D,     ALBBCK   ,  &
             EMISS         ,JULDAY               ,  &
             eprod2d,l_pv_up2d,l_pv_down2d,         &
             frc_pv2d, em_pv_up2d, pv_albedo2d,     &
             shade_frc2d,t_module2d, qconv2d,       &
             ids,ide,jds,jde,kds,kde,               &
             ims,ime,jms,jme,kms,kme,               &
             its,ite,jts,jte,kts,kte,               &
             pv_opt, id,i,j, DZ, ground_alb2d,     &
             ground_lai2d,   ground_emiss2d, ground_smois2d)


IMPLICIT NONE

! PV model based on Jones and Underwood (2001), Duffie and Beckman (2013) and
! Braun and Mitchell (1983)
! Modules needed later on


REAL, DIMENSION( ims:ime, jms:jme ), INTENT(IN) :: SWDDIF,&
                                                   OMG_URB2D,&
                                                   COSZ_URB2D,&
                                                   TSK,&
                                                   EMISS, &
                                                   ALBBCK, &
                                                   GLW, &
                                                   SWDOWN


REAL, DIMENSION( ims:ime, kms:kme, jms:jme ),  INTENT(IN) :: T3D,&
                                                            U_PHY,&
                                                            V_PHY,&
                                                            P8w3D,&
                                                             DZ

INTEGER,  INTENT(IN)   ::           ids,ide, jds,jde, kds,kde,  &
                                    ims,ime, jms,jme, kms,kme,  &
                                    its,ite, jts,jte, kts,kte


real, DIMENSION( ims:ime, jms:jme ), INTENT(INOUT) :: eprod2d    ,  &
                                                      l_pv_up2d  ,  &
                                                      l_pv_down2d,  &
                                                      frc_pv2d   ,  &
                                                      shade_frc2d,  &
                                                      t_module2d ,  &
                                                      ground_alb2d, &
                                                      ground_lai2d, &
                                                      ground_emiss2d,& 
                                                      ground_smois2d,& 
                                                      qconv2d

real, dimension(ims:ime, jms:jme), intent(out) :: em_pv_up2d, pv_albedo2d 

INTEGER, INTENT(IN) :: JULDAY, pv_opt, id
INTEGER ::  i ,j
REAL, INTENT(IN) :: DECLIN_URB

real :: frc_pv


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Declare parameters and variables  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! PV characteristics

real, PARAMETER :: alpha = 0.94            ! absorptivity of the cell surface
real, PARAMETER :: absorptivity = 0.77     ! based on that 77 % of photons are in the energy range that can be absorbed by silicon (Underwood 2001)
real, PARAMETER :: PV_albedo_min = 0.05    ! reflectivity of the PV surface
real, PARAMETER :: n = 1.526               ! index of the refraction of the cover glass
real, PARAMETER :: K_glazing = 4           ! glazing extinction coefficient (m-1)
real, PARAMETER :: h = 1.33            ! PV height above ground (of horizontal axis, m)
real, PARAMETER :: L = 1.956           ! length of PV module perpendicular to tilt axis (m)
real, PARAMETER :: W = 0.941            ! width of PV module, necessary for calculating Pout and 2 axes shade fraction calc
real, PARAMETER :: glass_thickness = 0.006  ! thickness of glazing layer (m)
real em_module_down
real em_module_up
real ZLVL

! tracking system related constants
real, PARAMETER :: beta_max = 60.           ! maximum tilt angle of PV (horixontal 1-axis tracking system)
real, PARAMETER :: beta_night = 30.         ! tilt angle of PV at night (horixontal 1-axis tracking system)
real, PARAMETER :: beta_tilt = 15.          ! tilt angle for non-tracking and vertical axis tracking
real, PARAMETER :: gamma_h_1 = 0.         ! azimuth angle of fixed, tilted system (0 facing equator, west 90, east -90)
real, PARAMETER :: beta_dash = 15.          ! tilt angle of sloped axis (sloped 1-axis tracking system)
real, PARAMETER :: gamma_sloped = 0.        ! azimuth angle of sloped axis (0 facing equator, west 90, east -90)

! Universal constants
real, PARAMETER :: sigma = 5.669*10.0**(-8)  ! Stefan-Boltzmann constant
real, PARAMETER :: Rv = 461.0              ! Gas constant for water vapour (JK‐1 kg-1)

! PV power output
real, PARAMETER :: Eff_PV = 0.16           ! conversion efficiency of the PV module
real, PARAMETER :: CFF = 1.22              ! fill factor model constant (K m^2)
real, PARAMETER :: k1 = 10**6              ! constant=K/I0 (m^2 K)

! PV array
real, PARAMETER :: width_rows = 5.64   ! distance between rows (measured from one axis to the other, m)
real, PARAMETER :: length_rows = 200   ! length of PV panel rows in array (m)

! these could come from WRF? AMB
! Bird solar model parameters
real, PARAMETER :: Ozone = 0.3
real, PARAMETER :: H2O = 1.5
real, PARAMETER :: Taua = 0.08
real, PARAMETER :: Ba = 0.85

real :: PI

!!!!!!!!!!!!!!!
!   Switches  !
!!!!!!!!!!!!!!!
integer, PARAMETER :: tracking_type = 3 ! 1=flat, 2=tilted, fixed, 3= horizontal tracking, 4=sloped axis tracking, 5= vertical axis tracking, 6= 2-axis tracking
integer, PARAMETER :: PV_type = 1 ! 1 = Polychrystalline, 0 = Monocrystalline
integer, PARAMETER :: Insulated = 0 ! 1 = insulated, 0 = not insulated
integer, PARAMETER :: Power_mod = 1 ! 1 = Masson, don't change in Fortran version
integer, PARAMETER :: A = 1 ! surface area
real :: cell, glass, backing, insulation
real :: Cmodule
real :: beta_PV, gamma_h

real :: THEATAZ
real :: THEATAS    ! = PI/2. - THETAZ
real :: cos_theta_zh, theta_zh, gamma_sh

real :: cos_theta_h_dash, gamma_zero, sigma_gamma_one, sigma_gamma_two

real, DIMENSION(3) :: cross
real, dimension(3) :: P1, P2, P3, P4
real, dimension(3) :: P11, P21, P31, P41
real, dimension(3,3) :: R_z, R_y, R_x
real, dimension(3,4) :: point_matrix, rotated_matrix
real, dimension(3) :: p111,p222,p333,ps1, ps2, ps3, ps4
real, dimension(3) :: s,b,c,nn, d_interm
real :: dot_A, dot_B, c_interm
real :: shade_area, ground_area
real :: theta_h_degree, theta_h_rad, theta_r, tau_b, tau_nil
real :: IAM_b, theta_h_dif_degree, theta_h_dif_rad, IAM
real :: tau_dif, IAM_dif_sky, theta_h_difg_degree, theta_h_difg_rad
real :: tau_difg, IAM_difg

real :: theta_zh_degree, AM, M
real :: total_rad

real :: A_EW, B_EW, AAA, BBB, S1, S2, U, U_EW, S_EW, psi
real :: VF_angle1, VF_angle2, VF_angle
real :: width_rows_psi, phi, VF_angle_psi1, VF_angle_psi2
real :: VFangle_psi, PVF_PV, SVF_PV, GVF_PV, PAF
real :: alpha1, alpha_rad, beta, beta_0, beta_rad, beta_zero_dash
real :: cos_gamma_sh, cos_solar_alt, cos_theta_h
real :: direct_normal_rad, gamma_1, gamma_rad, hc_j, hc_kum
real :: hc_sharp, hc_test, l_char_kum, l_char_sharp
real :: l_char,l_char_test
real :: l_shd 
real :: sigma_beta, sigma_beta_dash,cos_eps
real :: sin_gamma_sh,gamma_sh1, sin_solar_alt, total_rad_clear_theoretical
real :: t_change, theta_h

real :: p_out, qconv, l_pv_up 
real :: qconv_1, qconv_2, qconv_3, qconv_4, qlw
real :: l_down, l_ground, l_pv_down, l_pv_in, l_ratio
real :: KupPV
real :: PV_albedo 
!real :: em_pv_up

real :: module_in, shade_frc
real ::  OMG, DECLIN, COSZ, LLG, TA, DELT 
real :: SSGQ, SSGD, SSG 
real :: T_ground,UA,UAA,press,ground_albedo,em_ground,soil_moisture,ground_lai
integer :: doy, ipv, jpv
real :: xlatt

real :: GH,Kdif_model
real :: ETR, Air_mass, T_rayleigh, T_ozone, T_gases, T_water, T_aerosol, TAA
real :: rs, ld, las, ldnh
real :: r_perp_unpol,r_par_unpol, total_rad_temp, r_refl

integer :: starttime = 0 ! generally 0
real :: deltatfrc ! timestep in hours
integer :: numfrc, timefrc_index, k
real :: counter
real :: timeis, timeend
real :: inputdt ! NEEDED?
real :: outpt_tm !
logical :: first_write
real :: Tnew,Told,Fold,Fold_prime,qlw_ext
real :: hpv

integer :: kt, itf,jtf, ktf


PI = 4.0*ATAN(1.0)


if (PV_type == 1) then
    cell = A*0.00038*2330*712 ! Thickness (m), density (kg/m3), specific heat capacity (J/(kg*K))
    glass = A*glass_thickness*2500*840
    backing = A*0.00017*1475*1130
    insulation = A*0.1016*55*1210
else
    cell = A*0.00086*2330*712
    glass = A*glass_thickness*2500*840
    backing = A*0.00017*1475*1130
    insulation = A*0.1016*55*1210
end if

! Heat capacity of PV module
if (Insulated == 1) then
    Cmodule = cell+backing+glass+insulation
else
    Cmodule = cell+backing+glass
end if

! Characteristic length for sensible heat flux calculation
L_char = 4*L*W/(2*L+2*W)

! AMB don't need these?
outpt_tm = 1800.0 ! Output time step


DO kt = 1,nt

IF ( pv_opt .eq. 1 ) THEN


i = ival(kt,id)
j = jval(kt,id)

frc_pv = frcpv_1D(kt)
em_module_down = em_module_down_1D(kt)
em_module_up   = em_module_up_1D(kt)

ground_albedo  = ground_alb_1D(kt)
soil_moisture  = ground_smois_1D(kt)
em_ground = ground_emiss_1D(kt)
ground_lai  = ground_lai_1D(kt)

itf=MIN0(ite,ide-1)
jtf=MIN0(jte,jde-1)
ktf=MIN0(kte,kde-1)


!! make sure all the inputs from the old version are represented here...
if (i.ne.-9999.and.j.ne.-9999) then
  IF (( its .LE. i .AND. i .LE. itf ) .AND. &
   ( jts .LE. j .AND. j .LE. jtf )  ) THEN

        ssgq = SWDDIF(I,J)
        ssgd = swdown(i,j) - ssgq
        ssg  = swdown(i,j)
        if (ssgq .gt. swdown(i,j) ) then
            ssgq = swdown(i,j)
            ssgd = 0.0
        endif
        UAA = SQRT(U_PHY(I,1,J)**2.+V_PHY(I,1,J)**2.)
        omg = OMG_URB2D(I,J) * 180./ (4.0*ATAN(1.0))
        if (omg > 180.) then
           omg = omg - 360.
        endif
        if (omg < -180.) then
           omg = omg + 360.
        endif

        cosz = COSZ_URB2D(I,J)
        declin = DECLIN_URB * 180./ (4.0*ATAN(1.0))
  !      xlat_pv = XLAT(i,j)
        doy = JULDAY
  !      dt_pv = dt
        press = P8w3D(i,1,j)/100.
        module_in = t_module2d(i,j)
        TA = T3D(i,1,j) 
        t_ground = tsk(i,j) 
       ! ground_albedo = ALBBCK(I,J)
        LLG =GLW(I,J) 
        ZLVL = DZ(i,1,j)*0.5
        hpv = 2.0 ! height of PV
        UA = 2./pi * log(h/0.25)/log((zlvl-h) + h/0.25) * UAA



! Initial values:
timeis=starttime ! current number of hours
timeend=starttime+deltatfrc*real(numfrc) ! total number of hours
timefrc_index=2


THEATAS = ABS(ASIN(COSZ)) ! COSZ input from WRF
THEATAZ = ABS(ACOS(COSZ))

!if Power_mod == 'Sandia':
!    AA,NcellSer, Isc0, Imp0, Vmp0, aIsc, aImp, C0, C1, BVmp, DiodeFactor, C2,
!    C3, a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, b5, DELT0, fd, a, b =
!    sandia_parameter_selection()

if (SSG < 0.) then
    SSG = 0.
end if

!! cosine of the zenit angle
cos_theta_zh = COSZ
!cos(XLAT*PI/180)*cos(DECLIN*PI/180)*cos(OMG*PI/180)+sin(DECLIN*PI/180)*sin(XLAT*PI/180)

! zenit angle (degrees)
theta_zh = acos(cos_theta_zh)/PI*180
! hourly solar azimuth angle (degrees)
gamma_sh = asin((sin(OMG*PI/180)*cos(DECLIN*PI/180))/sin(theta_zh*PI/180))/PI*180

! if gamma_sh is NaN set gamma_sh to -90 - Jannik fix
if (gamma_sh /= gamma_sh) then
    gamma_sh = -90.0
endif


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   PV tracking types (after Braun and Mitchell 1983) !
! 0 = fixed, horizontal                               !
! 1 = fixed, tilted                                   !
! 2 = horizontal 1-axis tracking                      !
! 3 = slope 1-axis tracking                           !
! 4 = vertical 1-axis tracking                        !
! 5 = 2-axes tracking                                 !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (tracking_type == 1) then ! flat
        beta_PV = 0.
        gamma_h = 0. !azimuth of PV module
end if
if (tracking_type == 2) then ! tilted, fixed
        beta_PV = beta_tilt
        gamma_h = gamma_h_1
end if
if (tracking_type ==3) then
        ! for tracking system with horizontal axis, which is north-south
        ! oriented
        if (OMG < 0.) then
            gamma_h = -90.
        else
            gamma_h = 90.
        end if

        ! tilt angle (after Braun and Mitchell 1983)
        ! this is for a north-south oriented horizontal axis (panels tilt
        ! east-west)

        beta_0 = atan(tan(theta_zh*PI/180)*cos((gamma_h-gamma_sh)*PI/180))/PI*180

        if (beta_0 >= 0.) then
            sigma_beta = 0.
        else
            sigma_beta = 1.
        end if

        beta_PV = beta_0 + sigma_beta*180.

        if (beta_PV > beta_max) then
            beta_PV = beta_max
        end if
        if (cos_theta_zh<0.) then
            beta_PV = beta_night
        end if
end if

if (tracking_type == 4) then ! 1-axis sloped
      ! cos of incidence angle: angle between a direct solar ray and the surface
      ! normal
      cos_theta_h_dash =cos(theta_zh*PI/180)*cos(beta_dash*PI/180)+sin(theta_zh*PI/180)&
      &*sin(beta_dash*PI/180)*cos((gamma_sh-gamma_sloped)*PI/180)

      gamma_zero = gamma_sloped + atan((sin(theta_zh*PI/180)*sin((gamma_sh-gamma_sloped)&
      *PI/180))/(cos_theta_h_dash*sin(beta_dash*PI/180)))/PI*180

      if ((gamma_zero-gamma_sloped)*(gamma_sh-gamma_sloped)>=0.) then
          sigma_gamma_one = 0.
      else
          sigma_gamma_one = 1.
      end if
      if ((gamma_sh-gamma_sloped)>=0) then
          sigma_gamma_two = 1.
      else
          sigma_gamma_two = -1.
      end if
      gamma_h = gamma_zero + sigma_gamma_one*sigma_gamma_two*180

      beta_zero_dash = atan(tan(beta_dash*PI/180)/cos((gamma_h-gamma_sloped)*PI/180))/PI*180

      if (beta_zero_dash>=0.0) then
          sigma_beta_dash = 0.
      else
          sigma_beta_dash  = 1.
      end if
      beta_PV = beta_zero_dash + sigma_beta_dash*180
end if
    if (tracking_type == 5) then ! 1-axis vertical
        gamma_h = gamma_sh
        beta_PV = beta_tilt
    end if

    if (tracking_type == 6) then ! two-axis
        if (cos_theta_zh>0) then
            beta_PV = theta_zh
            gamma_h = gamma_sh
        else
            beta_PV = 30.
            gamma_h = 0.
        end if
    end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Start shade calculation                                                     !
!Define the geometry and orientation of the PV module as a plane in 3D space  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Coordinates of points at corners
! height is adjusted later on
P1 = (/L/2.0, -W/2.0, 0.0/)
P2 = (/L/2.0,W/2.0,0.0/)
P3 = (/-L/2.0,W/2.0,0.0/)
P4 = (/-L/2.0,-W/2,0.0/)

! Rotate plane with rotation matrizes R_z, R_y, R_x
alpha1 = gamma_h ! counterclockwise rotation about z-axis (gamma_h)
beta = beta_PV ! counterclockwise rotation about y-axis (beta_PV)
gamma_1 = 0.0 ! counterclockwise rotation about x -axis ()



if (tracking_type == 3) then
	alpha1 = 0.0
	beta = 0.0
	if (gamma_h <= 0.0) then
		gamma_1 = beta_PV
	else
		gamma_1 = -beta_PV
	end if
end if

if (tracking_type == 4) then
    cos_eps = cos(beta_PV*PI/180)/cos(beta_dash*PI/180)
	eps = acos(cos_eps)*180/PI
	alpha1 = 0.0
    beta = beta_dash
    if (gamma_h <= 0.0) then
        gamma_1 = eps
    else
        gamma_1 = -eps
    end if
end if

alpha_rad = alpha1*PI/180.0
beta_rad = beta*PI/180.0
gamma_rad = gamma_1*PI/180.0

R_z =RESHAPE((/cos(alpha_rad),sin(alpha_rad),0.0,-sin(alpha_rad),cos(alpha_rad),0.0,0.0,0.0,1.0/),(/3,3/))
R_y =RESHAPE((/cos(beta_rad),0.0,-sin(beta_rad),0.0,1.0,0.0,sin(beta_rad),0.0,cos(beta_rad)/),(/3,3/))
R_x =RESHAPE((/1.0,0.0,0.0,0.0,cos(gamma_rad),sin(gamma_rad),0.0,-sin(gamma_rad),cos(gamma_rad)/),(/3,3/))


point_matrix =RESHAPE((/P1(1),P1(2),P1(3),P2(1),P2(2),P2(3),P3(1),P3(2),P3(3),P4(1),P4(2),P4(3)/),(/3,4/))

rotated_matrix = MATMUL(R_y,point_matrix)
rotated_matrix = MATMUL(R_z,rotated_matrix)
rotated_matrix = MATMUL(R_x,rotated_matrix)

rotated_matrix(3,:) =  rotated_matrix(3,:) + h ! lift plane up to the correct height

! Corner points of the correctly oriented PV panel
P11 = rotated_matrix(:,1)
P21 = rotated_matrix(:,2)
P31 = rotated_matrix(:,3)
P41 = rotated_matrix(:,4)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   Calculate shadow on ground surface                      !
!  (here also tilted ground surfaces could be implemented)  !
!   for now the ground surface is assumed to be horizontal  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! define points for ground surface plane
p111 = (/1,0,0/)
p222 = (/0,1,0/)
p333 = (/0,0,0/)

cos_solar_alt = cos(THEATAS)
sin_solar_alt = sin(THEATAS)
cos_gamma_sh = cos(gamma_sh*PI/180.0)
sin_gamma_sh = sin(gamma_sh*PI/180.0)

s = (/cos_solar_alt * cos_gamma_sh, cos_solar_alt * sin_gamma_sh, sin_solar_alt/)

b = p222-p111      ! direction vector of ground plane
c = p333-p111      ! direction vector of ground planei

!  Calculate cross product
cross(1) = b(2) * c(3) - b(3) * c(2)
cross(2) = b(3) * c(1) - b(1) * c(3)
cross(3) = b(1) * c(2) - b(2) * c(1)

!nn = cross ! normal vectr of plane

dot_A = DOT_PRODUCT(cross,P11-p111)
dot_B = DOT_PRODUCT(cross,s)
c_interm = dot_A/dot_B
d_interm = c_interm * s
ps1 = P11-d_interm

dot_A = DOT_PRODUCT(cross,P21-p111)
dot_B = DOT_PRODUCT(cross,s)
c_interm = dot_A/dot_B
d_interm = c_interm * s
ps2 = P21-d_interm

dot_A = DOT_PRODUCT(cross,P31-p111)
dot_B = DOT_PRODUCT(cross,s)
c_interm = dot_A/dot_B
d_interm = c_interm * s
ps3 = P31-d_interm

dot_A = DOT_PRODUCT(cross,P41-p111)
dot_B = DOT_PRODUCT(cross,s)
c_interm = dot_A/dot_B
d_interm = c_interm * s
ps4 = P41-d_interm

shade_area = abs(((ps1(1)*ps2(2)-ps1(2)*ps2(1))+(ps2(1) &
    *ps3(2)-ps2(2)*ps3(1))+(ps3(1)*ps4(2)-ps3(2)*ps4(1))+ &
    (ps4(1)*ps1(2)-ps4(2)*ps1(1)))/2.0)

!PV_area =
!abs(((P11(1)*P21(2)-P11(2)*P21(1))+(P21(1)*P31(2)-P21(2)*P31(1))+(P31(1)*P41(2)-P31(2)*P41(1))+(P41(1)*P11(2)-P41(2)*P11(1)))/2);

if (tracking_type < 4) then
    ground_area = width_rows * W
    shade_frc = shade_area/ground_area
else
    ground_area = width_rows * width_rows
    shade_frc = shade_area/ground_area
end if

if (shade_frc >1) then
    shade_frc = 1
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! End of shade fraction calculation !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! calculate cos of incidence angle: angle between a direct solar ray and the
! surface normal
cos_theta_h =cos(theta_zh*PI/180.0)*cos(beta_PV*PI/180.0)+sin(theta_zh*PI/180.0)*&
    sin(beta_PV*PI/180.0)*cos((gamma_sh-gamma_h)*PI/180.0)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculate global radiation for cloud free conditions after Bird 1984 !
! this is used as a safety measure at sunrise/sunset                   !
! for total radiation calc later on                                    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ETR=1367.0*(1.00011+0.034221*cos(2.0*PI*(doy-1.0)/365.0)+0.00128*sin(2.0*PI*(doy-1.0)/365.0)+0.000719*&
    cos(2.0*(2.0*PI*(doy-1.0)/365.0))+0.000077*sin(2.0*(2.0*PI*(doy-1.0)/365.0)))

if (theta_zh<89.0) then
        Air_mass = 1.0/(cos(theta_zh/(180.0/PI))+0.15/(93.885-theta_zh)**1.25)
    else
        Air_mass = 0.0
end if

if (Air_mass >0.0) then
    T_rayleigh = exp(-0.0903*(press*Air_mass/1013.0)**0.84*(1.0+press*Air_mass/1013.0-(press*Air_mass/1013.0)**1.01))
    T_ozone =1.0-0.1611*(Ozone*Air_mass)*(1.0+139.48*(Ozone*Air_mass))**-0.3034-0.002715*&
    (Ozone*Air_mass)/(1.0+0.044*(Ozone*Air_mass)+0.0003*(Ozone*Air_mass)**2.0)
    T_gases = exp(-0.0127*(Air_mass*press/1013.0)**0.26)
    T_water =1.0-2.4959*Air_mass*H2O/((1.0+79.034*H2O*Air_mass)**0.6828+6.385*H2O*Air_mass)
    T_aerosol = exp(-(Taua**0.873)*(1.0+Taua-Taua**0.7088)*Air_mass**0.9108)
    TAA = 1.0-0.1*(1.0-Air_mass+Air_mass**1.06)*(1.0-T_aerosol)
    rs = 0.0685+(1.0-Ba)*(1.0-T_aerosol/TAA)
    ld = 0.9662*ETR*T_aerosol*T_water*T_gases*T_ozone*T_rayleigh
    las =ETR*cos(theta_zh/(180.0/PI))*0.79*T_ozone*T_gases*T_water*TAA*(0.5*(1.0-T_rayleigh)&
    +Ba*(1.0-(T_aerosol/TAA)))/(1.0-Air_mass+(Air_mass)**1.02)
end if

if (theta_zh < 90.0) then
    ldnh = ld*cos(theta_zh/(180.0/PI))
else
    ldnh = 0.0
end if

if (Air_mass > 0.0) then
     GH = (ldnh+las)/(1.0-ground_albedo*rs)
     Kdif_model = GH- ldnh
else
     GH = 0.0
     Kdif_model = 0.0
end if

! direct normal radiation
if (cos_theta_zh>0.0) then
    direct_normal_rad = (SSG-SSGQ)/cos_theta_zh
else
    direct_normal_rad = 0.0
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! model of incident angle refraction losses (reflectivity of glazing in
! dependence on incident angle) !
! Calculate PV reflectivity based on incidence angle after De Soto et al. (2006)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

theta_h_degree = acos(cos_theta_h)*180/PI

if (cos_theta_zh > 0.0) then
    ! for beam radiation
    theta_h_rad = acos(cos_theta_h)
    theta_r = asin(1/n*sin(theta_h_rad)) ! angle of refraction

    tau_b = exp(-((K_glazing*glass_thickness)/cos(theta_r)))*1.0-1.0/2.0* &
        ((sin(theta_r-theta_h_rad)**2.0)/(sin(theta_r+theta_h_rad)**2.0)+tan(theta_r-theta_h_rad)**2.0&
        /(tan(theta_r+theta_h_rad)**2.0))
    tau_nil = exp(-K_glazing*glass_thickness)*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_b = tau_b/tau_nil ! IAM for beam radiation

    ! for diffuse radiation from sky
    theta_h_dif_degree = 59.68 - 0.1388*beta_PV + 0.001497*beta_PV**2 !effective incidence angle, Duffie and Beckman 2013 p. 213
    theta_h_dif_rad = theta_h_dif_degree*PI/180.0
    theta_r = asin(1/n*sin(theta_h_dif_rad)) ! angle of refraction
    tau_dif = exp(-((K_glazing*glass_thickness)/cos(theta_r)))*1.0-1.0/2.0*((sin(theta_r-theta_h_dif_rad)**2.0) &
        /(sin(theta_r+theta_h_dif_rad)**2.0)+tan(theta_r-theta_h_dif_rad)**2.0/ &
        (tan(theta_r+theta_h_dif_rad)**2.0))
    tau_nil = exp(-K_glazing*glass_thickness)*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_dif_sky = tau_dif/tau_nil

    ! for diffuse radiation from ground
    theta_h_difg_degree = 90 - 0.5788*beta_PV + 0.002693*beta_PV**2 !effective incidence angle, Duffie and Beckman 2013 p. 213
    theta_h_difg_rad = theta_h_difg_degree*PI/180.0
    theta_r = asin(1/n*sin(theta_h_dif_rad)) ! angle of refraction
    tau_difg = exp(-((K_glazing*glass_thickness)/cos(theta_r)))*1.0-1.0/2.0*((sin(theta_r-theta_h_difg_rad)**2.0)&
        /(sin(theta_r+theta_h_difg_rad)**2.0)+tan(theta_r-theta_h_difg_rad)**2.0/&
        (tan(theta_r+theta_h_difg_rad)**2.0))
    tau_nil = exp(-K_glazing*glass_thickness)*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_difg = tau_difg/tau_nil

    ! reflectivity based on Fresnel's law
    theta_r = asin(1/n*sin(theta_h_rad)) ! angle of refraction (rad)
    r_perp_unpol = ((sin(theta_h_rad-theta_r))**2)/((sin(theta_r+theta_h_rad))**2)
    r_par_unpol  = ((tan(theta_h_rad-theta_r))**2)/((tan(theta_r+theta_h_rad))**2) 
    PV_albedo = (r_perp_unpol + r_par_unpol)/2

    if (gamma_sh == -90.0) then

        tau_b     = 1.0 - PV_albedo_min
        tau_dif   = 1.0 - PV_albedo_min
        tau_difg  = 1.0 - PV_albedo_min
        PV_albedo = PV_albedo_min
    
    endif

else
    IAM = 1.
    IAM_b = 1. ! IAM for beam radiation
    IAM_dif_sky = 1.
    IAM_difg = 1.
    PV_albedo = PV_albedo_min
    r_refl = 0.0
end if


! if pv_albedo is nan or greater than 0.10
if ((PV_albedo .lt. PV_albedo_min) .or. (PV_albedo .ne. PV_albedo) .or. &
    (PV_albedo .gt. 0.10) ) then
    PV_albedo = PV_albedo_min
endif 


theta_zh_degree = acos(cos_theta_zh)*180/PI

! Air mass after King et al. 1998
AM = 1/(cos_theta_zh+0.5057*(96.080-theta_zh_degree)**(-1.634))

! Air mass modifier after DeSoto et al. 2006
M =0.935823*AM**0.0+0.054289*AM**1.0+(-0.008677*AM**2.0)+0.000527*AM**3.0+(-0.000011*AM**4.0)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! total radiation on a tilted surface (reflectivity considered)   !
!           after Stackhouse 2016 p.26, Duffie and Beckman 2013   !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (cos_theta_zh>0.0) then
    if (SSGQ>SSG) then
        SSGQ = SSG-1.e-9
    end if
    total_rad_temp = (SSG-SSGQ)*(cos_theta_h/cos_theta_zh)*(1.0-PV_albedo)    &
    +SSGQ*((1.0+cos(beta_PV*PI/180.0))/2.0)*(1.0-PV_albedo)+SSG*ground_albedo &
    *((1.0-cos(beta_PV*PI/180.0))/2.0)*(1.0-PV_albedo)
    ! shortwave rad reaching cell surface after Duffie and Beckman 2013 p. 231
    total_rad = M*((SSG-SSGQ)*(cos_theta_h/cos_theta_zh)*tau_b*alpha+SSGQ* &
        ((1.0+cos(beta_PV*PI/180.0))/2.0)*tau_dif*alpha+SSG*ground_albedo*((1.0-cos(beta_PV*PI/180.0))&
        /2.0)*tau_difg*alpha)
    total_rad_clear_theoretical =(GH-Kdif_model)*(cos_theta_h/cos_theta_zh)+Kdif_model* &
        ((1.0+cos(beta_PV*PI/180.0))/2.0)+GH*ground_albedo*((1.0-cos(beta_PV*PI/180.0))/2.0)
    
    ! reflected shortwave radiation from PV AMB
    KupPV = ((SSG-SSGQ)*(cos_theta_h/cos_theta_zh)*PV_albedo) + (SSGQ*((1.0+cos(beta_PV*PI/180.0))/2.0)*PV_albedo)

    if (total_rad<0.0) then
        total_rad=0.0
        total_rad_temp = 0.0
       
        ! reflected shortwave radiation from PV AMB 
        KupPV = 0.0
    end if
    if (total_rad > total_rad_clear_theoretical) then 
! total radiation is not allowed to go above the theoretical total radiation for clear skies at the current time step
        total_rad = total_rad_clear_theoretical
        total_rad_temp = total_rad_clear_theoretical 
        
        ! reflected shortwave radiation from PV AMB
        KupPV = (SSGQ*((1.0+cos(beta_PV*PI/180.0))/2.0)*PV_albedo)       
 
        if (total_rad<SSGQ*((1.0+cos(beta_PV*PI/180.0))/2.0)) then
            total_rad=SSGQ*((1+cos(beta_PV*PI/180))/2)
            total_rad_temp =SSGQ*((1+cos(beta_PV*PI/180))/2) 
        end if
    end if
else ! i.e. if the sun is below the horizon
    total_rad=SSGQ
    total_rad_temp=SSGQ
    KupPV = (SSGQ*((1.0+cos(beta_PV*PI/180.0))/2.0)*PV_albedo) 
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculate view factors for PV                                 !
! (assuming a row of PVs in front and behind), sky and ground   !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

psi = 90.0-abs(gamma_sh) ! 0 when east, 0 when west, 90 when south

L_shd = abs(L/(cos(psi*PI/180.0)))

if (abs(gamma_sh)<2) then
     L_shd = L
end if

A_EW = sin(beta_PV*PI/180.0)*L/2.0
B_EW = sin(beta_PV*PI/180.0)*L

AAA = sin(beta_PV*PI/180.0)*L_shd/2.0
BBB = sin(beta_PV*PI/180.0)*L_shd
S1 = (h-AAA+BBB)/tan(THEATAS*PI/180.0)
S2 = (h-AAA)/tan(THEATAS*PI/180.0)
U = L_shd * cos(beta_PV*PI/180.0)
U_EW = L * cos(beta_PV*PI/180.0)
S_EW = abs(S1 * cos(psi*PI/180.0))

VF_angle1 = tan(B_EW/2.0/(U_EW/2.0+width_rows))*180.0/PI
VF_angle2 = tan(B_EW/2.0/(width_rows-U_EW/2.0))*180.0/PI
VF_angle = VF_angle1 + VF_angle2

width_rows_psi = sqrt(width_rows**2.0+(length_rows/2.0)**2.0)
phi = tan((length_rows/2.0)/width_rows)  ! angle (rad)
VF_angle_psi1 = atan((B_EW/2.0)/(U_EW+width_rows_psi))*180.0/PI
VF_angle_psi2 = atan((B_EW/2.0)/(width_rows_psi-U_EW))*180.0/PI
VFangle_psi = VF_angle_psi1 + VF_angle_psi2

VF_angle = (VF_angle+VFangle_psi)/2.0

PVF_PV = VF_angle/180.0 ! PV view fraction of the PV
SVF_PV = (180.0-VF_angle)/180.0 ! sky view fraction of the PV
GVF_PV = (180.0-VF_angle)/180.0! ground view fraction of the PV

! Calculate plan area fraction of PV needed later on
PAF = U/width_rows

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Longwave radiation transfer !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! weighted by the view fractions of the PV module, i.e. in a PV array each PV
! module partly "sees" the other modules in front and behind
! now implemented in "2.5D", later on we can think of implementing this in 3D

L_down = LLG * SVF_PV
L_PV_in =(PVF_PV/2.*sigma*em_module_up*module_in**4.+PVF_PV/2.*sigma*em_module_down*module_in**4.)
L_ground = sigma*em_ground*T_ground**4.*GVF_PV 
L_PV_up = sigma*em_module_up*module_in**4.
L_PV_down = sigma*em_module_down*module_in**4.

qlw = L_down + L_PV_in + L_ground+(1-em_ground)*L_down - L_PV_up - L_PV_down

!ESK:
qlw_ext=L_down + L_ground+(1-em_ground)*L_down

!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Convective heat transfer !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! ! !! Convective heat transfer TARP algorithm from EnergyPlus Engineering
! Reference p.45

hc_J =  4.6*exp(-0.6*UA)+6.137*UA**0.78 ! exact formula from Juerges 1924
qconv_1 = 2.0*( hc_J*(module_in-TA))

L_char_kum = 0.802                         ! characteristic length Kumar and Mullick 2010
L_ratio = L_char**(-0.2)/L_char_kum**(-0.2)
hc_Kum = 6.9 + 3.87*UA
qconv_2 = 2.0*(hc_Kum * (module_in-TA) * L_ratio) ! QH after Kumar and Mullick 2010

L_char_sharp = 1.193
L_ratio = L_char**(-0.2)/L_char_sharp**(-0.2)
hc_sharp = 3.3*UA+6.5
qconv_3 = 2.0*( hc_sharp * (module_in-TA) * L_ratio ) ! QH after Sharples and Charlesworth 1998

L_char_test = 0.976
L_ratio = L_char**(-0.2)/L_char_test**(-0.2)
hc_test = 2.56 * UA + 8.55
qconv_4 = 2.0*( hc_test * (module_in-TA) * L_ratio ) ! QH after Test et al 1981

qconv = (qconv_1 + qconv_2 + qconv_3 + qconv_4)/4.0 ! Average sensible heat flux correlations

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Electrical power generation                                !
!                                                            !
! currently only Masson model implemented in Fortran version !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (Power_mod == 1) then
    ! After Masson et al. 2014
    P_out = total_rad*Eff_PV*min(1.0,1.0-0.005*(module_in-298.15))
else if (Power_mod == 2) then
    theta_h = acos(cos_theta_h)/PI*180
    ! Sandia model
    !P_out = SandiaModel(total_rad-SSGQ,SSGQ+ground_albedo *
    !SSGQ*(sin(beta_PV*PI/180)),UA,TA-273.15,theta_zh,module_in-273.15,theta_h,AA,NcellSer,
    !Isc0, Imp0, Vmp0, aIsc, aImp, C0, C1, BVmp, DiodeFactor, C2, C3, a0, a1,
    !a2, a3, a4, b0, b1, b2, b3, b4, b5, DELT0, fd, a, b)

else if (Power_mod == 3) then
    !P_out = One_diode_Batzelis(total_rad, module_in)

else if (Power_mod == 4) then
    if (total_rad>0.0) then
      !Eff = Eff_PV*(1 - 0.00048*(module_in-25+275.15)+0.12*log(total_rad))
      !P_out = Eff * total_rad
    else
      P_out = 0.0
    end if
end if

!ESK:
Tnew=module_in
Told=Tnew+999.
do 899 while (abs(Tnew-Told).gt.0.001)
  Told=Tnew
  Fold=-qlw_ext-total_rad_temp*absorptivity+P_out+hc_J*(Told-TA)- &
         -(PVF_PV/2.*sigma*em_module_up*Told**4.+ &
           PVF_PV/2.*sigma*em_module_down*Told**4.) &
         +sigma*em_module_up*Told**4.+sigma*em_module_down*Told**4.
  Fold_prime=hc_J-(4.*PVF_PV/2.*sigma*em_module_up*Told**3.+ &
                   4.*PVF_PV/2.*sigma*em_module_down*Told**3.) &
                  +4.*sigma*em_module_up*Told**3. &
                  +4.*sigma*em_module_down*Told**3.
  Tnew=-Fold/Fold_prime+Told
899 continue

eprod2d(i,j) = p_out
l_pv_up2d(i,j) = l_pv_up
l_pv_down2d(i,j) = l_pv_down                              
frc_pv2d(i,j) = frc_pv                                                    
shade_frc2d(i,j) = shade_frc
t_module2d(i,j) = Tnew
qconv2d(i,j) = qconv
em_pv_up2d(i,j) = em_module_up
pv_albedo2d(i,j) = PV_albedo
ground_alb2d(i,j) = ground_albedo
ground_lai2d(i,j) = ground_lai
ground_smois2d(i,j) = soil_moisture
ground_emiss2d(i,j) = em_ground

endif
endif
endif
enddo

END SUBROUTINE PV_UCRC

 SUBROUTINE init_module_pv2(id, config_flags, xlat,xlong,pv_initialized,  &
               ims,ime,jms,jme,its,ite,jts,jte,ids,ide,jds,jde )
!
  IMPLICIT NONE
!
   integer ims,ime,jms,jme,ids,ide,jds,jde
   integer its,ite,jts,jte

   REAL,     DIMENSION( ims:ime , jms:jme ) , INTENT(IN) :: xlat, xlong
!   REAL,     DIMENSION( ims:ime , jms:jme ) , INTENT(INOUT) :: frc_pv2d
   TYPE (grid_config_rec_type) :: config_flags
   TYPE (PROJ_INFO) :: ts_proj
   logical :: pv_initialized
!
   CHARACTER*256 num,input,message_pv
   real lat,lon,ts_rx,ts_ry
   REAL :: known_lat, known_lon
   INTEGER :: i,j,k,id

   LOGICAL, EXTERNAL :: wrf_dm_on_monitor
!
    IF ( wrf_dm_on_monitor() ) THEN
!
! ... AMB: Opens the file with the location of the PV systems ...
!
        if ( config_flags%pv_ij .eq. 1 ) then
          open(75,file='pv_ij.txt',form='formatted',status='old')
        else
          open(75,file='pv.txt',form='formatted',status='old')
        end if
!
! ... AMB: Counts the turbines ...
!
       nt=0
 10    read(75,*,end=100)
       nt=nt+1
       goto 10
!
 100   continue
       rewind (75)
    END IF
!
     CALL wrf_dm_bcast_integer(nt,1)
!
! ... AMB: Initializes the configuration of the wind farm(s) ...
     if (.not. pv_initialized) then
       allocate (nkind(nt),ival(nt,max_domains),jval(nt,max_domains))
          allocate (frcpv_1D(nt),eff_pv_1D(nt),em_module_down_1D(nt),em_module_up_1D(nt),&
                    PV_albedo_min_1D(nt), ground_alb_1D(nt),ground_lai_1D(nt), ground_emiss_1D(nt), & 
                    ground_smois_1D(nt)   )
       ival=-9999
       jval=-9999
       pv_initialized=.true.
     endif
!
     IF ( wrf_dm_on_monitor() ) THEN
     do k=1,nt
       if ( config_flags%pv_ij .eq. 1 ) then
        read(75,*) ival(k,id), jval(k,id), nkind(k)
        write(message_pv,*)'PV Array #',k,': I, J = ',ival(k,id), jval(k,id),'; Type = ',nkind(k)
        CALL wrf_message(message_pv)

       else

         read(75,*)lat,lon,nkind(k)
         write(message_pv,*)'PV Array #',k,': Lat, lon = ',lat,lon, ' ; Type=',nkind(k)
         CALL wrf_message(message_pv)

         CALL map_init(ts_proj)

         known_lat = xlat(its,jts)
         known_lon = xlong(its,jts)

      ! Mercator
      IF (config_flags%map_proj == PROJ_MERC) THEN
         CALL map_set(PROJ_MERC, ts_proj,               &
                      truelat1 = config_flags%truelat1, &
                      lat1     = known_lat,             &
                      lon1     = known_lon,             &
                      knowni   = REAL(its),             &
                      knownj   = REAL(jts),             &
                      dx       = config_flags%dx)

      ! Lambert conformal
      ELSE IF (config_flags%map_proj == PROJ_LC) THEN
        CALL map_set(PROJ_LC, ts_proj,                   &
                      truelat1 = config_flags%truelat1,  &
                      truelat2 = config_flags%truelat2,  &
                      stdlon   = config_flags%stand_lon, &
                      lat1     = known_lat,              &
                      lon1     = known_lon,              &
                      knowni   = REAL(its),              &
                      knownj   = REAL(jts),              &
                      dx       = config_flags%dx)
!      ! Polar stereographic
      ELSE IF (config_flags%map_proj == PROJ_PS) THEN
         CALL map_set(PROJ_PS, ts_proj,                  &
                      truelat1 = config_flags%truelat1,  &
                      stdlon   = config_flags%stand_lon, &
                      lat1     = known_lat,              &
                      lon1     = known_lon,              &
                      knowni   = REAL(its),              &
                      knownj   = REAL(jts),              &
                      dx       = config_flags%dx)
!
      END IF
!
         CALL latlon_to_ij(ts_proj, lat, lon, ts_rx, ts_ry)
!
          ival(k,id)=nint(ts_rx)
          jval(k,id)=nint(ts_ry)
          if (ival(k,id).lt.ids.and.ival(k,id).gt.ide) then
            ival(k,id)=-9999
            jval(k,id)=-9999
          endif
!
          write(message_pv,*)'PV Arraye #',k,': Lat, lon = ',lat,lon, &
                               ', (i,j) = (',ival(k,id),',',jval(k,id),') Type=',nkind(k)

          CALL wrf_debug(0,message_pv)
!
    end if
!
     enddo
      close(75)
    

! ... PAJ: Read the tables with the turbine's characteristics ...
    
      DO i=1,nt
          write(num,*) nkind(i)
          num=adjustl(num)
          input="pv-"//trim(num)//".tbl"
          OPEN(file=TRIM(input),unit=21,FORM='FORMATTED',STATUS='OLD')

          READ(21,*,ERR=132) frcpv_1D(i),eff_pv_1D(i),em_module_down_1D(i), &
               em_module_up_1D(i), PV_albedo_min_1D(i), ground_alb_1D(i),   & 
               ground_lai_1D(i), ground_emiss_1D(i), ground_smois_1D(i)
          close (21)
          
       ENDDO

 132   continue

      endif

        CALL wrf_dm_bcast_integer(ival,nt*max_domains)
        CALL wrf_dm_bcast_integer(jval,nt*max_domains)
        CALL wrf_dm_bcast_real(frcpv_1D,nt)
        CALL wrf_dm_bcast_real(eff_pv_1D,nt)
        CALL wrf_dm_bcast_real(em_module_down_1D,nt)
        CALL wrf_dm_bcast_real(em_module_up_1D,nt)
        CALL wrf_dm_bcast_real(PV_albedo_min_1D,nt)
        CALL wrf_dm_bcast_real(ground_alb_1D,nt)
        CALL wrf_dm_bcast_real(ground_lai_1D,nt)
        CALL wrf_dm_bcast_real(ground_emiss_1D,nt)
        CALL wrf_dm_bcast_real(ground_smois_1D,nt)
        CALL wrf_dm_bcast_integer(nkind,nt)

  END SUBROUTINE init_module_pv2

END MODULE module_sf_pv
