MODULE PV_UCRC

! PV model based on Jones and Underwood (2001)

IMPLICIT NONE

! the NOAH LSM has one ground surface temperature calculation which is modified by the vegetation fractions
! we might modify solar radiation in noahlsm and run it twice: for sunlit and shaded ground surface, and then average both according to the shaded fraction.
! The ground surface could then still be covered by vegetation

! import modules, files and all constants needed

!! Input variables
! global_rad: global radiation (W/m^2)
! doy: day of year (hourly)
! TA: ambient air temperature (K)
! u: wind velocity (m/s)
! T_ground: ground surface temperature (K)
! TZ: time zone relative to UTC
! XLAT: Latitude (in decimal degrees)
! Lon: Longitude (in decimal degrees)
! absorptivity: absorptivity of the PV module (dependent on the PV cell properties; may not be constant)
! em_ground: emissivity of the ground surface
! em_ground: emissivity of the PV module
! DELT: time step (s) --> DELT

! Initial conditions needed:
! T_module: temperature of the PV module (assumed to be uniform)

!!!!!!!!!!!!!!
! Constants  !
!!!!!!!!!!!!!!

! PV characteristics

real, PARAMETER :: absorptivity = 0.77    ! absorptivity of the cell surface
                                          ! based on that 77 % of photons are in the energy range that can be absorbed by silicon
real, PARAMETER :: PV_albedo_min = 0.05             ! reflectivity of the PV surface
real, PARAMETER :: n = 1.526               ! index of the refraction of the cover glass
real, PARAMETER :: K_glazing = 4           ! glazing extinction coefficient (m-1)
real, PARAMETER :: em_module_up = 0.82     ! PV module upper surface emissivity
real, PARAMETER :: em_module_down = 0.97   ! PV module lower surface emissivity
real, PARAMETER :: h = 1.33            ! PV height above ground (of horizontal axis, m)
real, PARAMETER :: L = 1.956           ! length of PV module perpendicular to tilt axis (m)
real, PARAMETER :: W = 0.941            ! width of PV module, necessary for calculating Pout and 2 axes shade fraction calc
real, PARAMETER :: L_char = 4*L*W/(2*L+2*W) ! characteristic length for sens. heat flux calculations (m)

! tracking system related constants
real, PARAMETER :: beta_max = 60.           ! maximum tilt angle of PV (horixontal 1-axis tracking system)
real, PARAMETER :: beta_night = 30.         ! tilt angle of PV at night (horixontal 1-axis tracking system)
real, PARAMETER :: beta_tilt = 15.          ! tilt angle for non-tracking and vertical axis tracking
real, PARAMETER :: gamma_h_1 = 0.         ! azimuth angle of fixed, tilted system (0 facing equator, west 90, east -90)
real, PARAMETER :: beta_dash = 15.          ! tilt angle of sloped axis (sloped 1-axis tracking system)
real, PARAMETER :: gamma_sloped = 0.        ! azimuth angle of sloped axis (0 facing equator, west 90, east -90)

! Location and time zone
!TZ = 7              ! MST relative to UTC

! Universal constants
real, PARAMETER :: sigma = 5.669*10**(-8)  ! Stefan-Boltzmann constant
real, PARAMETER :: Rv = 461.0              ! Gas constant for water vapour (J K‐1 kg-1)

! PV power output
real, PARAMETER :: Eff_PV = 0.16           ! conversion efficiency of the PV module
real, PARAMETER :: CFF = 1.22              ! fill factor model constant (K m^2)
real, PARAMETER :: k1 = 10**6              ! constant=K/I0 (m^2 K)

! PV array
real, PARAMETER :: width_rows = 5.64   ! distance between rows (measured from one axis to the other, m)
real, PARAMETER :: length_rows = 200   ! length of PV panel rows in array (m)


!!!!!!!!!!!!!!!
!   Switches  !
!!!!!!!!!!!!!!!
integer :: PV_type = 1 ! 1 = Polychrystalline, 0 = Monocrystalline
integer :: Insulated = 1 ! 1 = insulated, 0 = not insulated
integer :: A = 1 ! surface area
real :: cell, glass, backing, insulation
real:: Cmodule

if (PV_type == 1) then
    cell = A*0.00038*2330*712
    glass = A*0.006*2500*840
    backing = A*0.00017*1475*1130
    insulation = A*0.1016*55*1210
else
    cell = A*0.00086*2330*712
    glass = A*0.006*2500*840
    backing = A*0.00017*1475*1130
    insulation = A*0.1016*55*1210
endif

! Heat capacity of PV module
if (Insulated == 1) then
    Cmodule = cell+backing+glass+insulation
else
    Cmodule = cell+backing+glass
endif

integer, PARAMETER :: Power_mod == 1 ! 1 = Masson

!!!!!!!!!!!!!!!!!!!!
! Functions needed !
!!!!!!!!!!!!!!!!!!!!

FUNCTION cross(a, b)
  INTEGER, DIMENSION(3) :: cross
  INTEGER, DIMENSION(3), INTENT(IN) :: a, b

  cross(1) = a(2) * b(3) - a(3) * b(2)
  cross(2) = a(3) * b(1) - a(1) * b(3)
  cross(3) = a(1) * b(2) - a(2) * b(1)
END FUNCTION cross



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Read and interpolate meteorological input to timestep
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!! Following input needed:
! global radiation (global_rad) --> SSG
! diffuse radiation (Kdif) --> SSGQ
! direct radiation (Kdir) --> SSGD
! solar hour angle (OMG) --> OMG
! solar altitude (THEATAS) --> THEATAS (rad)
! Solar zenith angle (theta_zh, deg) --> THEATAZ (rad)
! declination angle (DECLIN) --> DECLIN
! Longwave radiation from sky (W/m2, LLG_meas) --> LLG
! Air temperature at PV height (K, TA)   --> TA (K)
! relative humidity at PV height (%, RH)
! Ground surface temperature (K, T_ground) --> T1 (noahlsm - this seems to be the surface temp for different surfaces...)
! wind speed at PV height (m/s, u)  --> UA (m/s)
! Atmospheric pressure at PV height (mb, P) --> SFCPRS
! Latitude (deg, XLAT) --> XXLAT
! ground albedo(ground_albedo) --> ALBBRD (noahlsm)
! Longitude
! time step (dt) --> DELT
! day of year (doy)

real :: THEATAZ
real :: THEATAS    ! = PI/2. - THETAZ

THEATAS=ABS(ASIN(COSZ)) ! COSZ input from WRF
THEATAZ=ABS(ACOS(COSZ))

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! now resample and interpolate to model timestep  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! eventually not necessary in WRF? Time steps up to 360 s are stable in the PV model - eventually also more since our explicit soil temp model is not needed here!



!!!!!!!!!!!!!!!!!!!!!!!!
! Initialize variables !
!!!!!!!!!!!!!!!!!!!!!!!!
real :: Tmodule, Ts_shd, Ts_sun,Tm_sun, Tm_shd, T_ground
real :: PI=4.D0*ATAN(1.D0)

! initialize PV module temperature
T_module = TA(1)

!T_ground = (T_ground_sun(1)+T_ground_shade(1))/2.
T_ground = TA(1)

! initialize temperature profile for finite difference scheme
!Tsoil = TA(1)
!Tsun = (Tsoil)*nz
!Tshade = (Tsoil)*nz

! initialize some things
delta_Ts_sun =0.
delta_Tm_sun = 0.
delta_Ts_shade = 0.
delta_Tm_shade = 0.
Rn_sun_ground = 0.
Rn_shade_ground = 0.
Qg_sun = 0.
Qg_shade = 0.
Rn_sun_ground_vec = (1)*3
Rn_shade_ground_vec = (1)*3

steps = int(inputDELT/DELT)

!if Power_mod == 'Sandia':
!    AA,NcellSer, Isc0, Imp0, Vmp0, aIsc, aImp, C0, C1, BVmp, DiodeFactor, C2, C3, a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, b5, DELT0, fd, a, b = sandia_parameter_selection()

if (SSG < 0.) then
    SSG = 0.
endif


! cosine of the zenit angle
real :: cos_theta_zh = cos(XLAT*PI/180)*cos(DECLIN*PI/180)*cos(OMG*PI/180)+sin(DECLIN*PI/180)*sin(XLAT*PI/180)
! zenit angle (degrees)
real :: theta_zh = acos(cos_theta_zh)/PI*180
! hourly solar azimuth angle (degrees)
real :: gamma_sh = asin((sin(OMG*PI/180)*cos(DECLIN*PI/180))/sin(theta_zh*PI/180))/PI*180

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! run Bird model clear sky radiation model                                                      !
! this is used to constrain total radiation during sunrise and sunset                           !
! small "imbalances" between direct and diffuse rad can lead to large errors in total rad here  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Bird model parameters (WRF values might be used here)
real, PARAMETER :: Ozone = 0.3
real, PARAMETER :: H2O = 1.5
real, PARAMETER :: Taua = 0.08
real, PARAMETER :: Ba = 0.85

if (theta_zh<89.)then
    Air_mass = 1/(cos(theta_zh/(180/PI))+0.15/(93.885-theta_zh)**1.25)
else
    Air_mass = 0.
endif

if (Air_mass > 0.) then
    T_rayleigh = exp(-0.0903*(P*Air_mass/1013)**0.84*(1+P*Air_mass/1013-(P*Air_mass/1013)**1.01))
    T_ozone = 1-0.1611*(Ozone*Air_mass)*(1+139.48*(Ozone*Air_mass))**-0.3034-0.002715*(Ozone*Air_mass)/(1+0.044*(Ozone*Air_mass)+0.0003*(Ozone*Air_mass)**2)
    T_gases = exp(-0.0127*(Air_mass*P/1013)**0.26)
    T_water = 1-2.4959*Air_mass*H2O/((1+79.034*H2O*Air_mass)**0.6828+6.385*H2O*Air_mass)
    T_aerosol = exp(-(Taua**0.873)*(1+Taua-Taua**0.7088)*Air_mass**0.9108)
    TAA = 1-0.1*(1-Air_mass+Air_mass**1.06)*(1-T_aerosol)
    rs = 0.0685+(1-Ba)*(1-T_aerosol/TAA)
    ld = 0.9662*1367*cos_theta_zh*T_aerosol*T_water*T_gases*T_ozone*T_rayleigh
    las = 1367*cos_theta_zh*0.79*T_ozone*T_gases*T_water*TAA*(0.5*(1-T_rayleigh)+Ba*(1-(T_aerosol/TAA)))/(1-Air_mass+(Air_mass)**1.02)
else
    T_rayleigh = 0.
    T_ozone = 0.
    T_gases = 0.
    T_water = 0.
    T_aerosol = 0.
    TAA = 0.
    rs = 0.
    ld = 0.
    las = 0.
endif

if (theta_zh < 90.) then
    ldnh = ld*cos_theta_zh
else
    ldnh = 0.
endif

if (Air_mass > 0.) then
    GH = (ldnh+las)/(1-ground_albedo*rs)
    Kdif_model = GH- ldnh
!    DNI = (global_rad*(1-ground_albedo*rs)-las)/cos(theta_zh/(180/PI))
!    Direct_hor = DNI*cos(theta_zh/(180/PI))
!    Kdif = global_rad - Direct_hor
else
    GH = 0.
!    DNI = 0.
!    Direct_hor = 0.
!    Kdif = 0.
    Kdif_model = 0.
endif

!!!!!!!!!!!!!!!!!!!!!!
!   tracking types   !
!!!!!!!!!!!!!!!!!!!!!!
real :: beta_PV
real :: gamma_h

if (tracking_type == 0) then ! flat
        beta_PV = 0.
        gamma_h = 0. !azimuth of PV module
endif
if (tracking_type == 1) then ! tilted, fixed
        beta_PV = beta_tilt
        gamma_h = gamma_h_1
endif
if (tracking_type ==2) then
        ! for tracking system with horizontal axis, which is north-south oriented
        if (OMG < 0.) then
            gamma_h = -90.
        else
            gamma_h = 90.
        endif

        ! tilt angle (after Braun and Mitchell 1983)
        ! this is for a north-south oriented horizontal axis (panels tilt east-west)

        beta_0 = atan(tan(theta_zh*PI/180)*cos((gamma_h-gamma_sh)*PI/180))/PI*180

        if (beta_0 >= 0.) then
            sigma_beta = 0.
        else
            sigma_beta = 1.
        endif

        beta_PV = beta_0 + sigma_beta*180.

        if (beta_PV > beta_max) then
            beta_PV = beta_max
        endif
        if (cos_theta_zh<0.) then
            beta_PV = beta_night
        endif

if (tracking_type == 3) then ! 1-axis sloped
      ! cos of incidence angle: angle between a direct solar ray and the surface normal
      real :: cos_theta_h_dash = cos(theta_zh*PI/180)*cos(beta_dash*PI/180)+sin(theta_zh*PI/180)*sin(beta_dash*PI/180)*cos((gamma_sh-gamma_sloped)*PI/180)
      real :: gamma_zero = gamma_sloped + atan((sin(theta_zh*PI/180)*sin((gamma_sh-gamma_sloped)*PI/180))/(cos_theta_h_dash*sin(beta_dash*PI/180)))/PI*180
      real :: sigma_gamma_one
      real :: sigma_gamma_two

      if ((gamma_zero-gamma_sloped)*(gamma_sh-gamma_sloped)>=0.) then
          sigma_gamma_one = 0.
      else
          sigma_gamma_one = 1.
      endif
      if ((gamma_sh-gamma_sloped)>=0) then
          sigma_gamma_two = 1.
      else
          sigma_gamma_two = -1.
      endif
      gamma_h = gamma_zero + sigma_gamma_one*sigma_gamma_two*180

      beta_zero_dash = atan(tan(beta_dash*PI/180)/cos(gamma_h*PI/180))/PI*180

      if (beta_zero_dash>=0.0) then
          sigma_beta_dash = 0.
      else
          sigma_beta_dash  = 1.
      endif
      beta_PV = beta_zero_dash + sigma_beta_dash*180

    if (tracking_type == 4) then ! 1-axis vertical
        gamma_h = gamma_sh
        beta_PV = beta_tilt
    endif

    if (tracking_type == 5) then ! two-axis
        if (cos_theta_zh>0) then
            beta_PV = theta_zh
            gamma_h = gamma_sh
        else
            beta_PV = 30.
            gamma_h = 0.
        endif
    endif

! calculate shade fraction and shadow length etc
!shade_frc,U,S_length,S_length_EW,S2_EW,B_EW,S1_EW,L_shd,U_EW,cos_beta_shd,psi = shade_frc_fct(gamma_sh,beta_PV,THEATAS,cos_theta_zh)
!shade_frc = shade_frc_gen(gamma_h,gamma_sh,beta_PV,THEATAS)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Start shade calculation                                                     !
!Define the geometry and orienation of the PV module as a plane in 3D space   !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

real, dimension(3) :: P1
P1 = (/L/2, -W/2, 0/)
real, dimension(3) :: P2
P2 = (/L/2,W/2,0/)
real, dimension(3) :: P3
P3 = (/-L/2,W/2,0/)
real, dimension(3) :: P4
P4 = (/-L/2,-W/2,0/)

! Rotate plane with rotation matrizes R_z, R_y, R_x
alpha = -gamma_h + 270 ! counterclockwise rotation about z-axis (gamma_h)
beta = beta_PV ! counterclockwise rotation about y-axis (beta_PV)
gamma = 0 ! counterclockwise rotation about x -axis ()

if (tracking_type == 4) then
    alpha = 0
    gamma = beta_dash
    if (gamma_sh <= 0) then
        beta = beta_PV
    else
        beta = -beta_PV
    endif
endif

alpha_rad = alpha*PI/180
beta_rad = beta*PI/180
gamma_rad = gamma*PI/180

real, dimension(3,3) :: R_z
real, dimension(3,3) :: R_y
real, dimension(3,3) :: R_x
real, dimension(3,4) :: point_matrix

R_z = RESHAPE((/cos(alpha_rad),sin(alpha_rad),0,-sin(alpha_rad),cos(alpha_rad),0,0,0,1/),(/3,3/))

R_y = RESHAPE((/cos(beta_rad),0,-sin(beta_rad),0,1,0,sin(beta_rad),0,cos(beta_rad)/),(/3,3/))

R_x = RESHAPE((/1,0,0,0,cos(gamma_rad),sin(gamma_rad),0,-sin(gamma_rad),cos(gamma_rad)/),(/3,3/))

point_matrix = ((/P1(1),P1(2),P1(3),P2(1),P2(2),P2(3),P3(1),P3(2),P3(3),P4(1),P4(2),P4(3)/),(/3,4/))

rotated_matrix = MATMUL(R_y,point_matrix)
rotated_matrix = MATMUL(R_z,rotated_matrix)
rotated_matrix = MATMUL(R_x,rotated_matrix)

rotated_matrix(3,:) =  rotated_matrix(3,:) + h ! lift plane up to the correct height

P11 = rotated_matrix(:,1)
P21 = rotated_matrix(:,2)
P31 = rotated_matrix(:,3)
P41 = rotated_matrix(:,4)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   Calculate shadow on ground surface                      !
!  (here also tilted ground surfaces could be implemented)  !
!    for now the ground surface is assumed to be horizontal !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! azimuth angle: gamma_sh
gamma_sh = -gamma_sh + 270

! define points for ground surface plane
real, dimension(3) :: p111
real, dimension(3) :: p222
real, dimension(3) :: p333

p111 = (/1,0,0/)
p222 = (/0,1,0/)
p333 = (/0,0,0/)

cos_solar_alt = cos(THEATAS*PI/180)
sin_solar_alt = sin(THEATAS*PI/180)
cos_gamma_sh = cos(gamma_sh*PI/180)
sin_gamma_sh = sin(gamma_sh*PI/180)

! solar direction vector
real, dimension(3) :: s
real, dimension(3) :: b
real, dimension(3) :: c
real, dimension(3) :: n

s = (/cos_solar_alt * cos_gamma_sh, cos_solar_alt * sin_gamma_sh, sin_solar_alt/)

b = p222-p111      ! direction vector of ground plane
c = p333-p111      ! direction vector of ground plane

n = cross(b,c); ! normal vector of plane

real :: dot_A = DOT_PRODUCT(n,P11-p111)
real :: dot_B = DOT_PRODUCT(n,s)
real :: c_interm = dot_A/dot_B
real :: d_interm = c_interm * s
real :: ps1 = P11-d_interm

dot_A = DOT_PRODUCT(n,P21-p111)
dot_B = DOT_PRODUCT(n,s)
c_interm = dot_A/dot_B
d_interm = c_interm * s
real :: ps2 = P21-d_interm

dot_A = DOT_PRODUCT(n,P31-p1)
dot_B = DOT_PRODUCT(n,s)
c_interm = dot_A/dot_B
d_interm = c_interm * s
real :: ps3 = P31-d_interm

dot_A = DOT_PRODUCT(n,P41-p1)
dot_B = DOT_PRODUCT(n,s)
c_interm = dot_A/dot_B
d_interm = c_interm * s
real :: ps4 = P41-d_interm;

real:: shade_area
shade_area = abs(((ps1(1)*ps2(2)-ps1(2)*ps2(1))+(ps2(1)*ps3(2)-ps2(2)*ps3(1))+(ps3(1)*ps4(2)-ps3(2)*ps4(1))+(ps4(1)*ps1(2)-ps4(2)*ps1(1)))/2)

!PV_area = abs(((P11(1)*P21(2)-P11(2)*P21(1))+(P21(1)*P31(2)-P21(2)*P31(1))+(P31(1)*P41(2)-P31(2)*P41(1))+(P41(1)*P11(2)-P41(2)*P11(1)))/2);

real :: ground_area
real :: shade_frc

if (tracking_type < 3) then
    ground_area = width_rows * W
    shade_frc = shade_area/ground_area
else
    ground_area = width_rows * width_rows
    shade_frc = shade_area/ground_area
endif

if (shade_frc >1) then
    shade_frc = 1
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! End of shade fraction calculation !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! cos of incidence angle: angle between a direct solar ray and the surface normal
cos_theta_h = cos(theta_zh*PI/180)*cos(beta_PV*PI/180)+sin(theta_zh*PI/180)*sin(beta_PV*PI/180)*cos((gamma_sh-gamma_h)*PI/180)

! direct normal radiation
if (cos_theta_zh>0) then
    direct_normal_rad = (SSG-SSGQ)/cos_theta_zh
else
    direct_normal_rad = 0
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! model of incident angle refraction losses (reflectivity of glazing in dependence on incident angle) !
! Calculate PV reflectivity based on incidence angle after De Soto et al. (2006)                      !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

real :: theta_h_degree = acos(cos_theta_h)*180/PI

if (cos_theta_zh > 0.0) then
    ! for beam radiation
    theta_h_rad = acos(cos_theta_h)
    theta_r = asin(1/n*sin(theta_h_rad)) ! angle of refraction

    tau_b = exp(-((K_glazing*Polycrystalline('Glass')(0))/cos(theta_r)))*1-1/2*((sin(theta_r-theta_h_rad)**2)/(sin(theta_r+theta_h_rad)**2)+tan(theta_r-theta_h_rad)**2/(tan(theta_r+theta_h_rad)**2))
    tau_nil = exp(-K_glazing*Polycrystalline('Glass')(0))*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_b = tau_b/tau_nil ! IAM for beam radiation

    ! for diffuse radiation from sky
    theta_h_dif_degree = 59.68 - 0.1388*beta_PV + 0.001497*beta_PV**2
    theta_h_dif_rad = theta_h_dif_degree*PI/180
    theta_r = asin(1/n*sin(theta_h_dif_rad)) ! angle of refraction
    tau_dif = exp(-((K_glazing*Polycrystalline('Glass')(0))/cos(theta_r)))*1-1/2*((sin(theta_r-theta_h_dif_rad)**2)/(sin(theta_r+theta_h_dif_rad)**2)+tan(theta_r-theta_h_dif_rad)**2/(tan(theta_r+theta_h_dif_rad)**2))
    tau_nil = exp(-K_glazing*Polycrystalline('Glass')(0))*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_dif_sky = tau_dif/tau_nil

    ! for diffuse radiation from ground
    theta_h_difg_degree = 90 - 0.5788*beta_PV + 0.002693*beta_PV**2
    theta_h_difg_rad = theta_h_difg_degree*PI/180
    theta_r = asin(1/n*sin(theta_h_dif_rad)) ! angle of refraction
    tau_difg = exp(-((K_glazing*Polycrystalline('Glass')(0))/cos(theta_r)))*1-1/2*((sin(theta_r-theta_h_difg_rad)**2)/(sin(theta_r+theta_h_difg_rad)**2)+tan(theta_r-theta_h_difg_rad)**2/(tan(theta_r+theta_h_difg_rad)**2))
    tau_nil = exp(-K_glazing*Polycrystalline('Glass')(0))*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_difg = tau_difg/tau_nil

else
    IAM_b = 1. ! IAM for beam radiation
    IAM_dif_sky = 1.
    IAM_difg = 1.
endif

real :: theta_zh_degree = acos(cos_theta_zh)*180/PI

! Air mass after King et al. 1998
real:: AM = 1/(cos_theta_zh+0.5057*(96.080-theta_zh_degree)**(-1.634))

! Air mass modifier after DeSoto et al. 2006
real :: M = 0.935823*AM**0+0.054289*AM**1+-0.008677*AM**2+0.000527*AM**3+-0.000011*AM**4

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! total radiation on a tilted surface (reflectivity considered)   !
!           after Stackhouse 2016 p.26, Duffie and Beckman 1991   !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
real :: total_rad

if (cos_theta_zh>0.) then
    if (SSGQ>SSG) then
        SSGQ = SSG-1.e-9
    endif

    total_rad = M*((SSG-SSGQ)*(cos_theta_h/cos_theta_zh)*tau_b*alpha+SSGQ*((1+cos(beta_PV*PI/180))/2)*tau_dif*alpha+SSG*ground_albedo*((1-cos(beta_PV*PI/180))/2)*tau_difg*alpha)
    total_rad_clear_theoretical = (GH-Kdif_model)*(cos_theta_h/cos_theta_zh)+Kdif_model*((1+cos(beta_PV*PI/180))/2)+GH*ground_albedo*((1-cos(beta_PV*PI/180))/2)

    if (total_rad<0.) then
        total_rad=0.0
    endif
    if (total_rad > total_rad_clear_theoretical) then ! total radiation is not allowed to go above the theoretical total radiation for clear skies at the current time step
        total_rad = total_rad_clear_theoretical
    endif
else ! i.e. if the sun is below the horizon
    total_rad=SSGQ
endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! calculate Tsurf using OHM and force-restore or finite difference scheme !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculate view factors for PV (assuming a row of PVs in front and behind), sky and ground

real :: psi = 90-abs(gamma_sh) ! 0 when east, 0 when west, 90 when south

real :: A_EW = sin(beta_PV*PI/180)*L/2
real :: B_EW = sin(beta_PV*PI/180)*L

real :: A = sin(beta_PV*PI/180)*L_shd/2
real :: B = sin(beta_PV*PI/180)*L_shd
real :: S1 = (h-A+B)/tan(THEATAS*PI/180)
real :: S2 = (h-A)/tan(THEATAS*PI/180)
real :: U = L_shd * cos(beta_PV*PI/180)
real :: U_EW = L * cos(beta_PV*PI/180)
real :: S_EW = abs(S1 * cos(psi*PI/180))

real :: VF_angle1 = tan(B_EW/2/(U_EW/2+width_rows))*180/PI
real :: VF_angle2 = tan(B_EW/2/(width_rows-U_EW/2))*180/PI
real :: VF_angle = VF_angle1 + VF_angle2

real :: width_rows_psi = sqrt(width_rows**2+(length_rows/2)**2)
real :: phi = tan((length_rows/2)/width_rows)  ! angle (rad)
real :: VF_angle_psi1 = atan((B_EW/2)/(U_EW+width_rows_psi))*180/PI
real :: VF_angle_psi2 = atan((B_EW/2)/(width_rows_psi-U_EW))*180/PI
real :: VFangle_psi = VF_angle_psi1 + VF_angle_psi2

real :: VF_angle = (VF_angle+VFangle_psi)/2.

real :: PVF_PV = VF_angle/180. ! PV view fraction of the PV
real :: SVF_PV = (180.-VF_angle)/180. ! sky view fraction of the PV
real :: GVF_PV = (180-VF_angle)/180.! ground view fraction of the PV

! Calculate plan area fraction of PV needed later on
real :: PAF = U/width_rows

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Longwave radiation transfer !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! this is weighted now by the view fractions of the PV module, i.e. in a PV array each PV module partly "sees" the other modules in front and behind
! now implemented in "2.5D", later on we can think of implementing this in 3D

L_down = LLG * SVF_PV
L_PV_in = (PVF_PV/2.*sigma*em_module_up*T_module**4.+PVF_PV/2.*sigma*em_module_down*T_module**4.)!
L_ground = sigma*em_ground*T_ground**4.*GVF_PV !
L_PV_up = sigma*em_module_up*T_module**4.
L_PV_down = sigma*em_module_down*T_module**4.

qlw = L_down + L_PV_in + L_ground+(1-em_ground)*L_down - L_PV_up - L_PV_down

!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Convective heat transfer !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! ! !! Convective heat transfer TARP algorithm from EnergyPlus Engineering Reference p.45
! Rf = 1.00
! hf1 = 2.537*((2*L+2*W)*UA/(L*W))**(1/2)        ! windward
! hf2 = 2.537*1/2*((2*L+2*W)*UA/(L*W))**(1/2)    ! lee ward
! hf = hf1 + hf2
! !
! hn1 = 9.482*(abs(T_module-TA))**(1/3)/(7.283-abs(cos(beta_PV*PI/180)))
! hn2 = 1.810*(abs(T_module-TA))**(1/3)/(1.382+abs(cos(beta_PV*PI/180)))
! hn = hn1 + hn2
! hc = hf + hn
! qconv = hc * (T_module-TA)

hc_J =  4.6*exp(-0.6*UA)+6.137*UA**0.78 ! exact formula from Juerges 1924
qconv_1 = hc_J*(T_module-TA)

L_char_kum = 0.802                         ! characteristic length Kumar and Mullick 2010
L_ratio = L_char**(-0.2)/L_char_kum**(-0.2)
hc_Kum = 6.9 + 3.87*UA
qconv_2 = hc_Kum * (T_module-TA) * L_ratio ! QH after Kumar and Mullick 2010

L_char_sharp = 1.193
L_ratio = L_char**(-0.2)/L_char_sharp**(-0.2)
hc_sharp = 3.3*UA+6.5
qconv_3 = hc_sharp * (T_module-TA) * L_ratio ! QH after Sharples and Charlesworth 1998

L_char_test = 0.976
L_ratio = L_char**(-0.2)/L_char_test**(-0.2)
hc_test = 2.56 * UA + 8.55
qconv_4 = hc_test * (T_module-TA) * L_ratio ! QH after Test et al 1981

qconv = (qconv_1 + qconv_2 + qconv_3 + qconv_4)/4          ! Average sensible heat flux correlations
!qconv_max = max(qconv_1,qconv_2,qconv_3,qconv_4)
!qconv_min = min(qconv_1,qconv_2,qconv_3,qconv_4)
! qconv = qconv_max

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Electrical power generation
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (Power_mod == 1) then
    ! After Masson et al. 2014
    P_out = (total_rad-PV_albedo*total_rad)*Eff_PV*min(1,1-0.005*(T_module-298.15))
else if (Power_mod == 2) then
    theta_h = acos(cos_theta_h)/PI*180
    ! Sandia model
    P_out = SandiaModel(total_rad-SSGQ,SSGQ+ground_albedo * SSGQ*(sin(beta_PV*PI/180)),UA,TA-273.15,theta_zh,T_module-273.15,theta_h,AA,NcellSer, Isc0, Imp0, Vmp0, aIsc, aImp, C0, C1, BVmp, DiodeFactor, C2, C3, a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, b5, DELT0, fd, a, b)

else if (Power_mod == 3) then
    P_out = One_diode_Batzelis(total_rad, T_module)

else if (Power_mod == 4) then
    if (total_rad>0.0) then
      Eff = Eff_PV*(1 - 0.00048*(T_module-25+275.15)+0.12*log(total_rad))
      P_out = Eff * total_rad
    else
      P_out = 0.0
    end if
end if

! Module temperature based on Euler time stepping (needs small DELT)
T_change = (qlw+absorptivity*total_rad-P_out-qconv)/Cmodule  ! with absorption fraction calc after Duffie and Beckman 1991
T_module = T_module + DELT * T_change

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Comparison with Masson model !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!    f_PV_masson = L/width_rows
!    T_module_masson = TA + 0.021*SSG
!    P_out_masson = SSG*Eff_PV*min(1,1-0.005*(T_module_masson-298.15))
!    LW_up_masson = em_module_up*sigma*T_module_masson**4+(1-em_module_up)*L_down
!    LW_down_PV_masson = sigma*TA**4
!    QH_masson = ((1-PV_albedo)*SSG+L_down-LW_up_masson+L_ground-LW_down_PV_masson)-P_out_masson


END MODULE PV_UCRC
