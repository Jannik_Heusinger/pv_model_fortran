
! PV model based on Jones and Underwood (2001), Duffie and Beckman (2013) and Braun and Mitchell (1983)

! Modules needed later on
module my_functions
implicit none

contains
! Calculate cross product
FUNCTION cross(aa, bb)
    implicit none
    real, DIMENSION(3) :: cross
    real, DIMENSION(3), INTENT(IN) :: aa, bb

    cross(1) = aa(2) * bb(3) - aa(3) * bb(2)
    cross(2) = aa(3) * bb(1) - aa(1) * bb(3)
    cross(3) = aa(1) * bb(2) - aa(2) * bb(1)
END FUNCTION cross

end module my_functions

program PVmodel

use my_functions

IMPLICIT NONE

! the NOAH LSM has one ground surface temperature calculation which is modified by the vegetation fractions
! we might modify solar radiation in noahlsm and run it twice: for sunlit and shaded ground surface, and then average both according to the shaded fraction.
! The ground surface could then still be covered by vegetation

! import modules, files and all constants needed

!! Input variables
! global_rad: global radiation (W/m^2)
! doy: day of year (hourly)
! TA: ambient air temperature (K)
! u: wind velocity (m/s)
! T_ground: ground surface temperature (K)
! TZ: time zone relative to UTC
! XLAT: Latitude (in decimal degrees)
! Lon: Longitude (in decimal degrees)
! absorptivity: absorptivity of the PV module (dependent on the PV cell properties; may not be constant)
! em_ground: emissivity of the ground surface
! em_ground: emissivity of the PV module
! DELT: time step (s) --> DELT

! Initial conditions needed:
! T_module: temperature of the PV module (assumed to be uniform)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Declare parameters and variables  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! PV characteristics

real, PARAMETER :: alpha = 0.94            ! absorptivity of the cell surface
real, PARAMETER :: absorptivity = 0.77     ! based on that 77 % of photons are in the energy range that can be absorbed by silicon (Underwood 2001)
real, PARAMETER :: PV_albedo_min = 0.05        ! reflectivity of the PV surface
real, PARAMETER :: n = 1.526               ! index of the refraction of the cover glass
real, PARAMETER :: K_glazing = 4           ! glazing extinction coefficient (m-1)
real, PARAMETER :: em_module_up = 0.82     ! PV module upper surface emissivity
real, PARAMETER :: em_module_down = 0.97   ! PV module lower surface emissivity
real, PARAMETER :: h = 1.33            ! PV height above ground (of horizontal axis, m)
real, PARAMETER :: L = 1.956           ! length of PV module perpendicular to tilt axis (m)
real, PARAMETER :: W = 0.941            ! width of PV module, necessary for calculating Pout and 2 axes shade fraction calc
real, PARAMETER :: glass_thickness = 0.006  ! thickness of glazing layer (m)

! Location
real, PARAMETER :: XLAT = 32.55                ! Latitude (°)

! tracking system related constants
real, PARAMETER :: beta_max = 60.           ! maximum tilt angle of PV (horixontal 1-axis tracking system)
real, PARAMETER :: beta_night = 30.         ! tilt angle of PV at night (horixontal 1-axis tracking system)
real, PARAMETER :: beta_tilt = 15.          ! tilt angle for non-tracking and vertical axis tracking
real, PARAMETER :: gamma_h_1 = 0.         ! azimuth angle of fixed, tilted system (0 facing equator, west 90, east -90)
real, PARAMETER :: beta_dash = 15.          ! tilt angle of sloped axis (sloped 1-axis tracking system)
real, PARAMETER :: gamma_sloped = 0.        ! azimuth angle of sloped axis (0 facing equator, west 90, east -90)

! ground characteristics
real, PARAMETER :: em_ground = 0.95          ! Emissivity of the ground surface
real, PARAMETER :: ground_albedo = 0.3       ! Albedo of ground surface

! Location and time zone
!TZ = 7              ! MST relative to UTC

! Universal constants
real, PARAMETER :: sigma = 5.669*10.0**(-8.0)  ! Stefan-Boltzmann constant
real, PARAMETER :: Rv = 461.0              ! Gas constant for water vapour (J K‐1 kg-1)

! PV power output
real, PARAMETER :: Eff_PV = 0.16           ! conversion efficiency of the PV module
real, PARAMETER :: CFF = 1.22              ! fill factor model constant (K m^2)
real, PARAMETER :: k1 = 10**6.0              ! constant=K/I0 (m^2 K)

! PV array
real, PARAMETER :: width_rows = 5.64   ! distance between rows (measured from one axis to the other, m)
real, PARAMETER :: length_rows = 200   ! length of PV panel rows in array (m)

! Bird solar model parameters
real, PARAMETER :: Ozone = 0.3
real, PARAMETER :: H2O = 1.5
real, PARAMETER :: Taua = 0.08
real, PARAMETER :: Ba = 0.85

real :: T_ground
real :: PI=4.0*ATAN(1.0)

!!!!!!!!!!!!!!!
!   Switches  !
!!!!!!!!!!!!!!!
integer, PARAMETER :: tracking_type = 2 ! 0=flat, 1=tilted, fixed, 2= horizontal tracking, 3=sloped axis tracking, 4= vertical axis tracking, 5= 2-axis tracking
integer, PARAMETER :: PV_type = 1 ! 1 = Polychrystalline, 0 = Monocrystalline
integer, PARAMETER :: Insulated = 0 ! 1 = insulated, 0 = not insulated
integer, PARAMETER :: Power_mod = 1 ! 1 = Masson, don't change in Fortran version
integer, PARAMETER :: A = 1 ! surface area
real :: cell, glass, backing, insulation
real :: Cmodule
real :: beta_PV, gamma_h

real :: THEATAZ
real :: THEATAS    ! = PI/2. - THETAZ
real :: cos_theta_zh, theta_zh, gamma_sh

real :: cos_theta_h_dash, gamma_zero, sigma_gamma_one, sigma_gamma_two

real, dimension(3) :: P1, P2, P3, P4
real, dimension(3) :: P11, P21, P31, P41
real, dimension(3,3) :: R_z, R_y, R_x
real, dimension(3,4) :: point_matrix, rotated_matrix
real, dimension(3) :: p111,p222,p333,ps1, ps2, ps3, ps4
real, dimension(3) :: s,b,c,nn, d_interm
real :: dot_A, dot_B, c_interm
real :: shade_area, ground_area, shade_frc
real :: theta_h_degree, theta_h_rad, theta_r, tau_b, tau_nil
real :: IAM_b, theta_h_dif_degree, theta_h_dif_rad
real :: tau_dif, IAM_dif_sky, theta_h_difg_degree, theta_h_difg_rad
real :: tau_difg, IAM_difg

real :: theta_zh_degree, AM, M
real :: total_rad

real :: A_EW, B_EW, AAA, BBB, S1, S2, U, U_EW, S_EW, psi
real :: VF_angle1, VF_angle2, VF_angle
real :: width_rows_psi, phi, VF_angle_psi1, VF_angle_psi2
real :: VFangle_psi, PVF_PV, SVF_PV, GVF_PV, PAF
real :: alpha1, alpha_rad, beta, beta_0, beta_rad, beta_zero_dash
real :: cos_gamma_sh, cos_solar_alt, cos_theta_h, DELT
real :: direct_normal_rad, gamma_1, gamma_rad, hc_j, hc_kum
real :: hc_sharp, hc_test, l_char_kum, l_char_sharp
real :: l_char,l_char_test
real :: l_down, l_ground, l_pv_down, l_pv_in, l_pv_up, l_ratio
real :: l_shd, p_out, qconv, qconv_1
real :: qconv_2, qconv_3, qconv_4, qlw, sigma_beta, sigma_beta_dash
real :: sin_gamma_sh,gamma_sh1, sin_solar_alt, total_rad_clear_theoretical
real :: t_change, t_module, theta_h, ua
real :: SSG,SSGQ,OMG,DECLIN,COSZ,LLG,TA,GH,Kdif_model
real :: ETR, Air_mass, T_rayleigh, T_ozone, T_gases, T_water, T_aerosol, TAA
real :: rs, ld, las, ldnh, doy, press
real :: r_perp_unpol,r_par_unpol, PV_albedo, total_rad_temp

integer :: starttime = 0 ! generally 0
real :: deltatfrc ! timestep in hours
integer :: numfrc, timefrc_index, k
real :: counter
real :: timeis, timeend
real :: inputdt ! NEEDED?
real :: outpt_tm !
logical :: first_write

! Declare input variable arrays
real,allocatable,dimension(:) :: SSGfrc ! shortwave radiation (W m-2)
real,allocatable,dimension(:) :: SSGQfrc ! diffuse shortwave radiation (W m-2)
real,allocatable,dimension(:) :: OMGfrc ! solar hour angle (°)
real,allocatable,dimension(:) :: DECLINfrc ! Declination angle (°)
real,allocatable,dimension(:) :: COSZfrc ! cosine of solar zenith angle
real,allocatable,dimension(:) :: LLGfrc ! longwave downward radiation from sky (W m-2)
real,allocatable,dimension(:) :: TAfrc ! ambient air temperature (K)
real,allocatable,dimension(:) :: GHfrc ! modeled cloud free global radiation from Bird's model (W m-2)
real,allocatable,dimension(:) :: Kdif_modelfrc ! diffuse radiation from Bird model
real,allocatable,dimension(:) :: UAfrc ! wind speed (m/s)
real,allocatable,dimension(:) :: Tgroundfrc ! soil surface temperature (K)
real,allocatable,dimension(:) :: press_frc ! cosine of incident angle
real,allocatable,dimension(:) :: doy_frc ! day of year

real,allocatable,dimension(:) :: timefrc

! PV module material characteristics
! PV_type = 1 Monocrystalline
! PV_type = 2 Polycrystalline
if (PV_type == 1) then
    cell = A*0.00038*2330*712 ! Thickness (m), density (kg/m3), specific heat capacity (J/(kg*K))
    glass = A*glass_thickness*2500*840
    backing = A*0.00017*1475*1130
    insulation = A*0.1016*55*1210
else
    cell = A*0.00086*2330*712
    glass = A*glass_thickness*2500*840
    backing = A*0.00017*1475*1130
    insulation = A*0.1016*55*1210
end if

! Heat capacity of PV module
if (Insulated == 1) then
    Cmodule = cell+backing+glass+insulation
else
    Cmodule = cell+backing+glass
end if

! Characteristic length for sensible heat flux calculation
L_char = 4*L*W/(2*L+2*W)

DELT = 360.0  ! Model time step
outpt_tm = 1800.0 ! Output time step

! Open OUTPUT files:

open(unit=837,file='PV.out',status='unknown', &
                       form='formatted')

write(837,*)'Tmodule ','P_out ','Total rad ','Air mass mod ', 'SSG ', 'SSGQ ', 'cos_theta_h ', 'cos_theta_zh ', 'shade frac', 'QH'

! ATMOSPHERIC FORCING

!  open input atmospheric data file
open (981,file='PV_forcing.dat')

read(981,*)numfrc,starttime,deltatfrc

allocate(SSGfrc(1:numfrc+1))
allocate(SSGQfrc(1:numfrc+1))
allocate(OMGfrc(1:numfrc+1))
allocate(DECLINfrc(1:numfrc+1))
allocate(COSZfrc(1:numfrc+1))
allocate(LLGfrc(1:numfrc+1))
allocate(TAfrc(1:numfrc+1))
allocate(UAfrc(1:numfrc+1))
allocate(Tgroundfrc(1:numfrc+1))
allocate(doy_frc(1:numfrc+1))
allocate(press_frc(1:numfrc+1))
allocate(timefrc(1:numfrc+1))

do k=1,numfrc
read(981,*) SSGfrc(k),SSGQfrc(k),OMGfrc(k),DECLINfrc(k),COSZfrc(k),&
            &LLGfrc(k),TAfrc(k), UAfrc(k), Tgroundfrc(k),&
            doy_frc(k), press_frc(k)

timefrc(k)=starttime+real(k-1)*deltatfrc
enddo

! initialize PV module temperature
T_module = TA

! Initial values:
timeis=starttime ! current number of hours
timeend=starttime+deltatfrc*real(numfrc) ! total number of hours
timefrc_index=2

counter = 1.0
do while (timeis.le.timeend)

   write(6,*)'TIMEIS = ',timeis

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! INTERPOLATE FORCING DATA  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   write(6,*)'deltatfrc',deltatfrc

   if (timefrc(timefrc_index).le.timeis)  &
                   timefrc_index=min(numfrc+1,timefrc_index+1)
    SSG=SSGfrc(timefrc_index-1)+(timeis  &
                        -timefrc(timefrc_index-1))  &
          /deltatfrc*(SSGfrc(timefrc_index)-SSGfrc(timefrc_index-1))
    SSGQ=SSGQfrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(SSGQfrc(timefrc_index)-SSGQfrc(timefrc_index-1))
    OMG=OMGfrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(OMGfrc(timefrc_index)-OMGfrc(timefrc_index-1))
    TA= Tafrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(Tafrc(timefrc_index)-Tafrc(timefrc_index-1))
    DECLIN=DECLINfrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(DECLINfrc(timefrc_index)-DECLINfrc(timefrc_index-1))
! UA [m/s]   : Wind speed at 1st atmospheric level
    COSZ=COSZfrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(COSZfrc(timefrc_index)-COSZfrc(timefrc_index-1))
    LLG=LLGfrc(timefrc_index-1)+(timeis-timefrc(timefrc_index-1))  &
          /deltatfrc*(LLGfrc(timefrc_index)  &
                        -LLGfrc(timefrc_index-1))
    UA=UAfrc(timefrc_index-1)+(timeis  &
                        -timefrc(timefrc_index-1))  &
          /deltatfrc*(UAfrc(timefrc_index)  &
                        -UAfrc(timefrc_index-1))
    T_ground=Tgroundfrc(timefrc_index-1)+(timeis  &
                        -timefrc(timefrc_index-1))  &
          /deltatfrc*(Tgroundfrc(timefrc_index)  &
                        -Tgroundfrc(timefrc_index-1))
    doy=doy_frc(timefrc_index-1)+(timeis  &
                        -timefrc(timefrc_index-1))  &
          /deltatfrc*(doy_frc(timefrc_index)  &
                        -doy_frc(timefrc_index-1))
    press =  press_frc(timefrc_index-1)+(timeis  &
                        -timefrc(timefrc_index-1))  &
          /deltatfrc*( press_frc(timefrc_index)  &
                        - press_frc(timefrc_index-1))

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Read and interpolate meteorological input to timestep
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!! Following input needed:
! global radiation (global_rad) --> SSG
! diffuse radiation (Kdif) --> SSGQ
! direct radiation (Kdir) --> SSGD
! solar hour angle (OMG) --> OMG
! solar altitude (THEATAS) --> THEATAS (rad)
! Solar zenith angle (theta_zh, deg) --> THEATAZ (rad)
! declination angle (DECLIN) --> DECLIN
! Longwave radiation from sky (W/m2, LLG_meas) --> LLG
! Air temperature at PV height (K, TA)   --> TA (K)
! relative humidity at PV height (%, RH)
! Ground surface temperature (K, T_ground) --> T1 (noahlsm - this seems to be the surface temp for different surfaces...)
! wind speed at PV height (m/s, u)  --> UA (m/s)
! Atmospheric pressure at PV height (mb, P) --> SFCPRS
! Latitude (deg, XLAT) --> XXLAT
! ground albedo(ground_albedo) --> ALBBRD (noahlsm)
! Longitude
! time step (dt) --> DELT
! day of year (doy)

THEATAS = ABS(ASIN(COSZ)) ! COSZ input from WRF
THEATAZ = ABS(ACOS(COSZ))

!if Power_mod == 'Sandia':
!    AA,NcellSer, Isc0, Imp0, Vmp0, aIsc, aImp, C0, C1, BVmp, DiodeFactor, C2, C3, a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, b5, DELT0, fd, a, b = sandia_parameter_selection()

if (SSG < 0.) then
    SSG = 0.
end if

!! cosine of the zenit angle
cos_theta_zh = COSZ !cos(XLAT*PI/180)*cos(DECLIN*PI/180)*cos(OMG*PI/180)+sin(DECLIN*PI/180)*sin(XLAT*PI/180)

! zenit angle (degrees)
theta_zh = acos(cos_theta_zh)/PI*180
! hourly solar azimuth angle (degrees)
gamma_sh = asin((sin(OMG*PI/180)*cos(DECLIN*PI/180))/sin(theta_zh*PI/180))/PI*180

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   PV tracking types (after Braun and Mitchell 1983) !
! 0 = fixed, horizontal                               !
! 1 = fixed, tilted                                   !
! 2 = horizontal 1-axis tracking                      !
! 3 = slope 1-axis tracking                           !
! 4 = vertical 1-axis tracking                        !
! 5 = 2-axes tracking                                 !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (tracking_type == 0) then ! flat
        beta_PV = 0.
        gamma_h = 0. !azimuth of PV module
end if
if (tracking_type == 1) then ! tilted, fixed
        beta_PV = beta_tilt
        gamma_h = gamma_h_1
end if
if (tracking_type ==2) then
        ! for tracking system with horizontal axis, which is north-south oriented
        if (OMG < 0.) then
            gamma_h = -90.
        else
            gamma_h = 90.
        end if

        ! tilt angle (after Braun and Mitchell 1983)
        ! this is for a north-south oriented horizontal axis (panels tilt east-west)

        beta_0 = atan(tan(theta_zh*PI/180)*cos((gamma_h-gamma_sh)*PI/180))/PI*180

        if (beta_0 >= 0.) then
            sigma_beta = 0.
        else
            sigma_beta = 1.
        end if

        beta_PV = beta_0 + sigma_beta*180.

        if (beta_PV > beta_max) then
            beta_PV = beta_max
        end if
        if (cos_theta_zh<0.) then
            beta_PV = beta_night
        end if
end if

if (tracking_type == 3) then ! 1-axis sloped
      ! cos of incidence angle: angle between a direct solar ray and the surface normal
      cos_theta_h_dash = cos(theta_zh*PI/180)*cos(beta_dash*PI/180)+sin(theta_zh*PI/180)&
      &*sin(beta_dash*PI/180)*cos((gamma_sh-gamma_sloped)*PI/180)

      gamma_zero = gamma_sloped + atan((sin(theta_zh*PI/180)*sin((gamma_sh-gamma_sloped)&
      &*PI/180))/(cos_theta_h_dash*sin(beta_dash*PI/180)))/PI*180

      if ((gamma_zero-gamma_sloped)*(gamma_sh-gamma_sloped)>=0.) then
          sigma_gamma_one = 0.
      else
          sigma_gamma_one = 1.
      end if
      if ((gamma_sh-gamma_sloped)>=0) then
          sigma_gamma_two = 1.
      else
          sigma_gamma_two = -1.
      end if
      gamma_h = gamma_zero + sigma_gamma_one*sigma_gamma_two*180

      beta_zero_dash = atan(tan(beta_dash*PI/180)/cos(gamma_h*PI/180))/PI*180

      if (beta_zero_dash>=0.0) then
          sigma_beta_dash = 0.
      else
          sigma_beta_dash  = 1.
      end if
      beta_PV = beta_zero_dash + sigma_beta_dash*180
end if
    if (tracking_type == 4) then ! 1-axis vertical
        gamma_h = gamma_sh
        beta_PV = beta_tilt
    end if

    if (tracking_type == 5) then ! two-axis
        if (cos_theta_zh>0) then
            beta_PV = theta_zh
            gamma_h = gamma_sh
        else
            beta_PV = 30.
            gamma_h = 0.
        end if
    end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Start shade calculation                                                     !
!Define the geometry and orientation of the PV module as a plane in 3D space  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Coordinates of points at corners
! height is adjusted later on
P1 = (/L/2.0, -W/2.0, 0.0/)
P2 = (/L/2.0,W/2.0,0.0/)
P3 = (/-L/2.0,W/2.0,0.0/)
P4 = (/-L/2.0,-W/2,0.0/)

! Rotate plane with rotation matrizes R_z, R_y, R_x
alpha1 = -gamma_h + 270.0 ! counterclockwise rotation about z-axis (gamma_h)
beta = beta_PV ! counterclockwise rotation about y-axis (beta_PV)
gamma_1 = 0.0 ! counterclockwise rotation about x -axis ()

if (tracking_type == 4) then
    alpha1 = 0.0
    gamma_1 = beta_dash
    if (gamma_sh <= 0.0) then
        beta = beta_PV
    else
        beta = -beta_PV
    end if
end if

alpha_rad = alpha1*PI/180.0
beta_rad = beta*PI/180.0
gamma_rad = gamma_1*PI/180.0

R_z = RESHAPE((/cos(alpha_rad),sin(alpha_rad),0.0,-sin(alpha_rad),cos(alpha_rad),0.0,0.0,0.0,1.0/),(/3,3/))
R_y = RESHAPE((/cos(beta_rad),0.0,-sin(beta_rad),0.0,1.0,0.0,sin(beta_rad),0.0,cos(beta_rad)/),(/3,3/))
R_x = RESHAPE((/1.0,0.0,0.0,0.0,cos(gamma_rad),sin(gamma_rad),0.0,-sin(gamma_rad),cos(gamma_rad)/),(/3,3/))


point_matrix = RESHAPE((/P1(1),P1(2),P1(3),P2(1),P2(2),P2(3),P3(1),P3(2),P3(3),P4(1),P4(2),P4(3)/),(/3,4/))

rotated_matrix = MATMUL(R_y,point_matrix)
rotated_matrix = MATMUL(R_z,rotated_matrix)
rotated_matrix = MATMUL(R_x,rotated_matrix)

rotated_matrix(3,:) =  rotated_matrix(3,:) + h ! lift plane up to the correct height

! Corner points of the correctly oriented PV panel
P11 = rotated_matrix(:,1)
P21 = rotated_matrix(:,2)
P31 = rotated_matrix(:,3)
P41 = rotated_matrix(:,4)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   Calculate shadow on ground surface                      !
!  (here also tilted ground surfaces could be implemented)  !
!   for now the ground surface is assumed to be horizontal  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! azimuth angle: gamma_sh
gamma_sh1 = -gamma_sh + 270.0

! define points for ground surface plane
p111 = (/1,0,0/)
p222 = (/0,1,0/)
p333 = (/0,0,0/)

cos_solar_alt = cos(THEATAS)
sin_solar_alt = sin(THEATAS)
cos_gamma_sh = cos(gamma_sh1*PI/180.0)
sin_gamma_sh = sin(gamma_sh1*PI/180.0)

! solar rays direction vector
s = (/cos_solar_alt * cos_gamma_sh, cos_solar_alt * sin_gamma_sh, sin_solar_alt/)


b = p222-p111      ! direction vector of ground plane
c = p333-p111      ! direction vector of ground plane


nn = cross(b,c); ! normal vector of plane

dot_A = DOT_PRODUCT(nn,P11-p111)
dot_B = DOT_PRODUCT(nn,s)
c_interm = dot_A/dot_B
d_interm = c_interm * s
ps1 = P11-d_interm

dot_A = DOT_PRODUCT(nn,P21-p111)
dot_B = DOT_PRODUCT(nn,s)
c_interm = dot_A/dot_B
d_interm = c_interm * s
ps2 = P21-d_interm

dot_A = DOT_PRODUCT(nn,P31-p1)
dot_B = DOT_PRODUCT(nn,s)
c_interm = dot_A/dot_B
d_interm = c_interm * s
ps3 = P31-d_interm

dot_A = DOT_PRODUCT(nn,P41-p1)
dot_B = DOT_PRODUCT(nn,s)
c_interm = dot_A/dot_B
d_interm = c_interm * s
ps4 = P41-d_interm

shade_area = abs(((ps1(1)*ps2(2)-ps1(2)*ps2(1))+(ps2(1) &
    *ps3(2)-ps2(2)*ps3(1))+(ps3(1)*ps4(2)-ps3(2)*ps4(1))+ &
    (ps4(1)*ps1(2)-ps4(2)*ps1(1)))/2.0)

!PV_area = abs(((P11(1)*P21(2)-P11(2)*P21(1))+(P21(1)*P31(2)-P21(2)*P31(1))+(P31(1)*P41(2)-P31(2)*P41(1))+(P41(1)*P11(2)-P41(2)*P11(1)))/2);

if (tracking_type < 3) then
    ground_area = width_rows * W
    shade_frc = shade_area/ground_area
else
    ground_area = width_rows * width_rows
    shade_frc = shade_area/ground_area
end if

if (shade_frc >1) then
    shade_frc = 1
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! End of shade fraction calculation !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! calculate cos of incidence angle: angle between a direct solar ray and the surface normal
cos_theta_h = cos(theta_zh*PI/180.0)*cos(beta_PV*PI/180.0)+sin(theta_zh*PI/180.0)*&
    sin(beta_PV*PI/180.0)*cos((gamma_sh-gamma_h)*PI/180.0)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculate global radiation for cloud free conditions after Bird 1984 !
! this is used as a safety measure at sunrise/sunset                   !
! for total radiation calc later on                                    !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

ETR =1367.0*(1.00011+0.034221*cos(2.0*PI*(doy-1.0)/365.0)+0.00128*sin(2.0*PI*(doy-1.0)/365.0)+0.000719*&
    cos(2.0*(2.0*PI*(doy-1.0)/365.0))+0.000077*sin(2.0*(2.0*PI*(doy-1.0)/365.0)))

if (theta_zh<89.0) then
        Air_mass = 1.0/(cos(theta_zh/(180.0/PI))+0.15/(93.885-theta_zh)**1.25)
    else
        Air_mass = 0.0
end if

if (Air_mass >0.0) then
    T_rayleigh = exp(-0.0903*(press*Air_mass/1013.0)**0.84*(1.0+press*Air_mass/1013.0-&
    (press*Air_mass/1013.0)**1.01))
    T_ozone = 1.0-0.1611*(Ozone*Air_mass)*(1.0+139.48*(Ozone*Air_mass))**-0.3034-0.002715*&
    (Ozone*Air_mass)/(1.0+0.044*(Ozone*Air_mass)+0.0003*(Ozone*Air_mass)**2.0)
    T_gases = exp(-0.0127*(Air_mass*press/1013.0)**0.26)
    T_water = 1.0-2.4959*Air_mass*H2O/((1.0+79.034*H2O*Air_mass)**0.6828+6.385*H2O*Air_mass)
    T_aerosol = exp(-(Taua**0.873)*(1.0+Taua-Taua**0.7088)*Air_mass**0.9108)
    TAA = 1.0-0.1*(1.0-Air_mass+Air_mass**1.06)*(1.0-T_aerosol)
    rs = 0.0685+(1.0-Ba)*(1.0-T_aerosol/TAA)
    ld = 0.9662*ETR*T_aerosol*T_water*T_gases*T_ozone*T_rayleigh
    las = ETR*cos(theta_zh/(180.0/PI))*0.79*T_ozone*T_gases*T_water*TAA*(0.5*(1.0-T_rayleigh)&
    +Ba*(1.0-(T_aerosol/TAA)))/(1.0-Air_mass+(Air_mass)**1.02)
end if

if (theta_zh < 90.0) then
    ldnh = ld*cos(theta_zh/(180.0/PI))
else
    ldnh = 0.0
end if

if (Air_mass > 0.0) then
     GH = (ldnh+las)/(1.0-ground_albedo*rs)
     Kdif_model = GH- ldnh
else
     GH = 0.0
     Kdif_model = 0.0
end if

! direct normal radiation
if (cos_theta_zh>0.0) then
    direct_normal_rad = (SSG-SSGQ)/cos_theta_zh
else
    direct_normal_rad = 0.0
end if

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! model of incident angle refraction losses (reflectivity of glazing in dependence on incident angle) !
! Calculate PV reflectivity based on incidence angle after De Soto et al. (2006)                      !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

theta_h_degree = acos(cos_theta_h)*180/PI

if (cos_theta_zh > 0.0) then
    ! for beam radiation
    theta_h_rad = acos(cos_theta_h)
    theta_r = asin(1/n*sin(theta_h_rad)) ! angle of refraction

    tau_b = exp(-((K_glazing*glass_thickness)/cos(theta_r)))*1.0-1.0/2.0* &
        ((sin(theta_r-theta_h_rad)**2.0)/(sin(theta_r+theta_h_rad)**2.0)+tan(theta_r-theta_h_rad)**2.0 &
        /(tan(theta_r+theta_h_rad)**2.0))
    tau_nil = exp(-K_glazing*glass_thickness)*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_b = tau_b/tau_nil ! IAM for beam radiation

    ! for diffuse radiation from sky
    theta_h_dif_degree = 59.68 - 0.1388*beta_PV + 0.001497*beta_PV**2 !effective incidence angle, Duffie and Beckman 2013 p. 213
    theta_h_dif_rad = theta_h_dif_degree*PI/180.0
    theta_r = asin(1/n*sin(theta_h_dif_rad)) ! angle of refraction
    tau_dif = exp(-((K_glazing*glass_thickness)/cos(theta_r)))*1.0-1.0/2.0*((sin(theta_r-theta_h_dif_rad)**2.0) &
        /(sin(theta_r+theta_h_dif_rad)**2.0)+tan(theta_r-theta_h_dif_rad)**2.0/ &
        (tan(theta_r+theta_h_dif_rad)**2.0))
    tau_nil = exp(-K_glazing*glass_thickness)*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_dif_sky = tau_dif/tau_nil

    ! for diffuse radiation from ground
    theta_h_difg_degree = 90 - 0.5788*beta_PV + 0.002693*beta_PV**2 !effective incidence angle, Duffie and Beckman 2013 p. 213
    theta_h_difg_rad = theta_h_difg_degree*PI/180.0
    theta_r = asin(1/n*sin(theta_h_dif_rad)) ! angle of refraction
    tau_difg = exp(-((K_glazing*glass_thickness)/cos(theta_r)))*1.0-1.0/2.0*((sin(theta_r-theta_h_difg_rad)**2.0) &
        /(sin(theta_r+theta_h_difg_rad)**2.0)+tan(theta_r-theta_h_difg_rad)**2.0/ &
        (tan(theta_r+theta_h_difg_rad)**2.0))
    tau_nil = exp(-K_glazing*glass_thickness)*(1-((1-n)/(1+n))**2) ! transmittance normal to sun
    IAM_difg = tau_difg/tau_nil

    ! reflectivity based on Fresnel's law
    theta_r = asin(1/n*sin(theta_h_rad)) ! angle of refraction (rad)
    r_perp_unpol = ((sin(theta_h_rad-theta_r))**2)/((sin(theta_r+theta_h_rad))**2)
    r_par_unpol = ((tan(theta_h_rad-theta_r))**2)/((tan(theta_r+theta_h_rad))**2)
    PV_albedo = (r_perp_unpol + r_par_unpol)/2

else
    IAM_b = 1. ! IAM for beam radiation
    IAM_dif_sky = 1.
    IAM_difg = 1.
    PV_albedo = PV_albedo_min
end if

theta_zh_degree = acos(cos_theta_zh)*180/PI

! Air mass after King et al. 1998
AM = 1/(cos_theta_zh+0.5057*(96.080-theta_zh_degree)**(-1.634))

! Air mass modifier after DeSoto et al. 2006
M = 0.935823*AM**0.0+0.054289*AM**1.0+(-0.008677*AM**2.0)+0.000527*AM**3.0+(-0.000011*AM**4.0)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! total radiation on a tilted surface (reflectivity considered)   !
!           after Stackhouse 2016 p.26, Duffie and Beckman 2013   !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (cos_theta_zh>0.0) then
    if (SSGQ>SSG) then
        SSGQ = SSG-1.e-9
    end if
    total_rad_temp = (SSG-SSGQ)*(cos_theta_h/cos_theta_zh)*(1.0-PV_albedo)&
    +SSGQ*((1.0+cos(beta_PV*PI/180.0))/2.0)*(1.0-PV_albedo)+SSG*ground_albedo&
    *((1.0-cos(beta_PV*PI/180.0))/2.0)*(1.0-PV_albedo)
    ! shortwave rad reaching cell surface after Duffie and Beckman 2013 p. 231
    total_rad = M*((SSG-SSGQ)*(cos_theta_h/cos_theta_zh)*tau_b*alpha+SSGQ* &
        ((1.0+cos(beta_PV*PI/180.0))/2.0)*tau_dif*alpha+SSG*ground_albedo*((1.0-cos(beta_PV*PI/180.0))&
        /2.0)*tau_difg*alpha)
    total_rad_clear_theoretical = (GH-Kdif_model)*(cos_theta_h/cos_theta_zh)+Kdif_model* &
        ((1.0+cos(beta_PV*PI/180.0))/2.0)+GH*ground_albedo*((1.0-cos(beta_PV*PI/180.0))/2.0)
    if (total_rad<0.0) then
        total_rad=0.0
        total_rad_temp = 0.0
    end if
    if (total_rad > total_rad_clear_theoretical) then ! total radiation is not allowed to go above the theoretical total radiation for clear skies at the current time step
        total_rad = total_rad_clear_theoretical
        total_rad_temp = total_rad_clear_theoretical
        if (total_rad<SSGQ*((1.0+cos(beta_PV*PI/180.0))/2.0)) then
                    total_rad=SSGQ*((1+cos(beta_PV*PI/180))/2)
                    total_rad_temp =SSGQ*((1+cos(beta_PV*PI/180))/2)
        end if
    end if
else ! i.e. if the sun is below the horizon
    total_rad=SSGQ
    total_rad_temp=SSGQ
end if
write(6,*) total_rad, total_rad_temp
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculate view factors for PV                                 !
! (assuming a row of PVs in front and behind), sky and ground   !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

psi = 90.0-abs(gamma_sh) ! 0 when east, 0 when west, 90 when south

L_shd = abs(L/(cos(psi*PI/180.0)))

if (abs(gamma_sh)<2) then
     L_shd = L
end if

A_EW = sin(beta_PV*PI/180.0)*L/2.0
B_EW = sin(beta_PV*PI/180.0)*L

AAA = sin(beta_PV*PI/180.0)*L_shd/2.0
BBB = sin(beta_PV*PI/180.0)*L_shd
S1 = (h-AAA+BBB)/tan(THEATAS*PI/180.0)
S2 = (h-AAA)/tan(THEATAS*PI/180.0)
U = L_shd * cos(beta_PV*PI/180.0)
U_EW = L * cos(beta_PV*PI/180.0)
S_EW = abs(S1 * cos(psi*PI/180.0))

VF_angle1 = tan(B_EW/2.0/(U_EW/2.0+width_rows))*180.0/PI
VF_angle2 = tan(B_EW/2.0/(width_rows-U_EW/2.0))*180.0/PI
VF_angle = VF_angle1 + VF_angle2

width_rows_psi = sqrt(width_rows**2.0+(length_rows/2.0)**2.0)
phi = tan((length_rows/2.0)/width_rows)  ! angle (rad)
VF_angle_psi1 = atan((B_EW/2.0)/(U_EW+width_rows_psi))*180.0/PI
VF_angle_psi2 = atan((B_EW/2.0)/(width_rows_psi-U_EW))*180.0/PI
VFangle_psi = VF_angle_psi1 + VF_angle_psi2

VF_angle = (VF_angle+VFangle_psi)/2.0

PVF_PV = VF_angle/180.0 ! PV view fraction of the PV
SVF_PV = (180.0-VF_angle)/180.0 ! sky view fraction of the PV
GVF_PV = (180.0-VF_angle)/180.0! ground view fraction of the PV

! Calculate plan area fraction of PV needed later on
PAF = U/width_rows

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Longwave radiation transfer !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! weighted by the view fractions of the PV module, i.e. in a PV array each PV module partly "sees" the other modules in front and behind
! now implemented in "2.5D", later on we can think of implementing this in 3D

L_down = LLG * SVF_PV
L_PV_in = (PVF_PV/2.*sigma*em_module_up*T_module**4.+PVF_PV/2.*sigma*em_module_down*T_module**4.)!
L_ground = sigma*em_ground*T_ground**4.*GVF_PV !
L_PV_up = sigma*em_module_up*T_module**4.
L_PV_down = sigma*em_module_down*T_module**4.

qlw = L_down + L_PV_in + L_ground+(1-em_ground)*L_down - L_PV_up - L_PV_down

!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Convective heat transfer !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! ! !! Convective heat transfer TARP algorithm from EnergyPlus Engineering Reference p.45
! Rf = 1.00
! hf1 = 2.537*((2*L+2*W)*UA/(L*W))**(1/2)        ! windward
! hf2 = 2.537*1/2*((2*L+2*W)*UA/(L*W))**(1/2)    ! lee ward
! hf = hf1 + hf2
! !
! hn1 = 9.482*(abs(T_module-TA))**(1/3)/(7.283-abs(cos(beta_PV*PI/180)))
! hn2 = 1.810*(abs(T_module-TA))**(1/3)/(1.382+abs(cos(beta_PV*PI/180)))
! hn = hn1 + hn2
! hc = hf + hn
! qconv = hc * (T_module-TA)

hc_J =  4.6*exp(-0.6*UA)+6.137*UA**0.78 ! exact formula from Juerges 1924
qconv_1 = hc_J*(T_module-TA)

L_char_kum = 0.802                         ! characteristic length Kumar and Mullick 2010
L_ratio = L_char**(-0.2)/L_char_kum**(-0.2)
hc_Kum = 6.9 + 3.87*UA
qconv_2 = hc_Kum * (T_module-TA) * L_ratio ! QH after Kumar and Mullick 2010

L_char_sharp = 1.193
L_ratio = L_char**(-0.2)/L_char_sharp**(-0.2)
hc_sharp = 3.3*UA+6.5
qconv_3 = hc_sharp * (T_module-TA) * L_ratio ! QH after Sharples and Charlesworth 1998

L_char_test = 0.976
L_ratio = L_char**(-0.2)/L_char_test**(-0.2)
hc_test = 2.56 * UA + 8.55
qconv_4 = hc_test * (T_module-TA) * L_ratio ! QH after Test et al 1981
write(6,*) T_module,TA

qconv = (qconv_1 + qconv_2 + qconv_3 + qconv_4)/4.0          ! Average sensible heat flux correlations
!qconv_max = max(qconv_1,qconv_2,qconv_3,qconv_4)
!qconv_min = min(qconv_1,qconv_2,qconv_3,qconv_4)
! qconv = qconv_max

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Electrical power generation                                !
!                                                            !
! currently only Masson model implemented in Fortran version !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

if (Power_mod == 1) then
    ! After Masson et al. 2014
    P_out = total_rad*Eff_PV*min(1.0,1.0-0.005*(T_module-298.15))
else if (Power_mod == 2) then
    theta_h = acos(cos_theta_h)/PI*180
    ! Sandia model
    !P_out = SandiaModel(total_rad-SSGQ,SSGQ+ground_albedo * SSGQ*(sin(beta_PV*PI/180)),UA,TA-273.15,theta_zh,T_module-273.15,theta_h,AA,NcellSer, Isc0, Imp0, Vmp0, aIsc, aImp, C0, C1, BVmp, DiodeFactor, C2, C3, a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, b5, DELT0, fd, a, b)

else if (Power_mod == 3) then
    !P_out = One_diode_Batzelis(total_rad, T_module)

else if (Power_mod == 4) then
    if (total_rad>0.0) then
      !Eff = Eff_PV*(1 - 0.00048*(T_module-25+275.15)+0.12*log(total_rad))
      !P_out = Eff * total_rad
    else
      P_out = 0.0
    end if
end if

! Module temperature based on Euler time stepping (needs small DELT)
T_change = (qlw+absorptivity*total_rad_temp-P_out-qconv)/Cmodule  ! with absorption fraction calc after Duffie and Beckman 1991
T_module = T_module + DELT * T_change

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Comparison with Masson model !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!    f_PV_masson = L/width_rows
!    T_module_masson = TA + 0.021*SSG
!    P_out_masson = SSG*Eff_PV*min(1,1-0.005*(T_module_masson-298.15))
!    LW_up_masson = em_module_up*sigma*T_module_masson**4+(1-em_module_up)*L_down
!    LW_down_PV_masson = sigma*TA**4
!    QH_masson = ((1-PV_albedo)*SSG+L_down-LW_up_masson+L_ground-LW_down_PV_masson)-P_out_masson

if (amod(counter,outpt_tm/DELT)==0) then
    first_write=.false.
    !call csv_write(837,T_module, P_out, total_rad, M, SSG, SSGQ, cos_theta_h, cos_theta_zh, shade_frc, qconv,.true.)
    write(837,844) T_module, P_out, total_rad, M, SSG, SSGQ, cos_theta_h, cos_theta_zh, shade_frc, qconv, cos_solar_alt
    write(6,*)'Tmodule = ',T_module
    write(6,*)'Tground = ',T_ground
end if
timeis=timeis+DELT/3600.
counter = counter + 1.0
844  format(11(1X,f7.3))

end do

end program PVmodel
