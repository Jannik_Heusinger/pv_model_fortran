#
"""
function to calculate the shaded fraction in a PV array (currently for horizontal tracking only)

# gamma_sh: solar azimuth angle
# beta_PV: tilt angle of PV module
# solar_alt: solar altitude (°)
# cos_theta_zh: cosine of the zenith angle


"""
from constants import *
from switches import *
import math


def shade_frc_fct(gamma_sh,beta_PV,solar_alt,cos_theta_zh):
# Shaded area fraction calculation
    psi = 90-abs(gamma_sh) # 0 when east, 0 when west, 90 when south
    #psi = gamma_sh

    L_shd = abs(L/(math.cos(psi*math.pi/180)))
    if abs(gamma_sh)<2:
        L_shd = L

    A_EW = math.sin(beta_PV*math.pi/180)*L/2
    B_EW = math.sin(beta_PV*math.pi/180)*L

    A = math.sin(beta_PV*math.pi/180)*L_shd/2
    B = math.sin(beta_PV*math.pi/180)*L_shd
    S1 = (h-A+B)/math.tan(solar_alt*math.pi/180)
    S2 = (h-A)/math.tan(solar_alt*math.pi/180)
    U = L_shd * math.cos(beta_PV*math.pi/180)
    U_EW = L * math.cos(beta_PV*math.pi/180)
    S_EW = abs(S1 * math.cos(psi*math.pi/180))
    cos_beta_shd = U/L_shd

    if cos_theta_zh > 0:
        S_length = abs(S1 +(U-S2))
        S_length_EW = abs((S_length-U_EW)*math.cos(psi*math.pi/180))+U_EW
        S1_EW = abs(S1*math.cos(psi*math.pi/180))
        S2_EW = abs(S2*math.cos(psi*math.pi/180))
        if S_length_EW >= width_rows: # width rows is currently defined in east-west direction. With tilted, fixed systems it needs to be defined in north-south direction
            shade_frc = 1 # shaded area fraction within the boundaries of the PV array
        elif S_length_EW>U_EW:
            shade_frc = S_length_EW/ width_rows
        else:
            shade_frc = U_EW/width_rows
    else:
        shade_frc = 0
        S_length = 0
        S_length_EW = 0
        S1_EW = 0
        S2_EW = 0
    if tracking_type == 2:
        width_shade = W
        shade_area = S_length * width_shade
        shade_frc = shade_area/(width_rows/2*width_rows/2)
        if shade_frc > 1:
            shade_frc = 1

    return shade_frc,U,S_length,S_length_EW,S2_EW,B_EW,S1_EW,L_shd,U_EW,cos_beta_shd,psi
